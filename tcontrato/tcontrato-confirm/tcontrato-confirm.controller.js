(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoConfirmarController', TcontratoConfirmarController);

    TcontratoConfirmarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TcontratoConfirmarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadConfirmarContrato();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarContrato() {

            UserService.ConfirmarContrato(vm.user.idUsuario)
            .then(function (response) {
                vm.confirmarcontrato=response;
            });
        } 
    }

})();
