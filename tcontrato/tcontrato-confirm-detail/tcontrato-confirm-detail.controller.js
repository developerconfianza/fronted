(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoConfirmarContratoController', TcontratoConfirmarContratoController);

    TcontratoConfirmarContratoController.$inject = ['$stateParams','$state','$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog'];
    function TcontratoConfirmarContratoController($stateParams,$state,$scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadConfirmarContratoDetalle();
        vm.ConfirmardetalledeContrato = ConfirmardetalledeContrato;
        vm.RechazardetalledeContrato=RechazardetalledeContrato;
     
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarContratoDetalle() {

            UserService.ConfirmarContratoDetalle(id)
            .then(function (response) {
                vm.confirmarcontratodetail=response.confirmado;
                $cookies.put('correocontratista', response.confirmado.correoUsuario);
                $cookies.put('nombrecontratista', response.confirmado.nombresUsuario);
                $cookies.put('apellidocontratista', response.confirmado.apellidosUsuario);
            });
        } 
        msgflash.clear();
        function ConfirmardetalledeContrato(){
            vm.dataLoading = true; 
            var correocontratista =  $cookies.get('correocontratista');
            var nombrecontratista =  $cookies.get('nombrecontratista');
            var apellidocontratista =  $cookies.get('apellidocontratista');
            var nombrecontratado =  vm.user.nombresUsuario;
            var apellidocontratado =  vm.user.apellidosUsuario;
            var correocontratado=vm.user.correoUsuario;
            var confirmado = {origen:id,correocontratista:correocontratista,nombrecontratista:nombrecontratista,
              apellidocontratista:apellidocontratista,nombrecontratado:nombrecontratado,apellidocontratado:apellidocontratado,
              correocontratado:correocontratado}; 

            UserService.ConfirmardetalledeContrato(confirmado)
            .then(function (response) {
                $state.go('tcontrato/tcontrato-confirm');
                msgflash.success('Se confirmo el contrato');

                vm.dataLoading= false;                  
            });
        }

        function RechazardetalledeContrato(){
            vm.dataLoading = true;  
            var correocontratista =  $cookies.get('correocontratista');
            var nombrecontratista =  $cookies.get('nombrecontratista');
            var apellidocontratista =  $cookies.get('apellidocontratista');
            var nombrecontratado =  vm.user.nombresUsuario;
            var apellidocontratado =  vm.user.apellidosUsuario;
            var correocontratado=vm.user.correoUsuario;
            var rechazado = {origen:id,correocontratista:correocontratista,nombrecontratista:nombrecontratista,
              apellidocontratista:apellidocontratista,nombrecontratado:nombrecontratado,apellidocontratado:apellidocontratado,
              correocontratado:correocontratado}; 
            UserService.RechazardetalledeContrato(rechazado)
            .then(function (response) {
                $state.go('tcontrato/tcontrato-confirm');
                msgflash.success('Se rechazo el contrato');

                vm.dataLoading= false;                  
            });
        }


        $scope.showConfirm = function(ev) {
          $scope.status = '  ';
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

          $mdDialog.show(confirm)
          .then(function() {
          ConfirmardetalledeContrato();
          },
          function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
          });
        };

      $scope.showRechazar = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        RechazardetalledeContrato();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };
    }

})();
