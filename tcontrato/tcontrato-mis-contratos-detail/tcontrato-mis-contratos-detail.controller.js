(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratomisContratoDetailController', TcontratomisContratoDetailController);

    TcontratomisContratoDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope'];
    function TcontratomisContratoDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope) {
        var vm = this;
        var moneda = {moneda:$stateParams.moneda };
        loadCurrentUser();
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            vm.nombre = $rootScope.globals.recibido;
        }

        Cargardetallecontrato();
        SetearNombreDetail(); 

        function Cargardetallecontrato() {
            var usuario= vm.user.idUsuario;
            var moneda = {moneda:$stateParams.moneda };
            var moneda = {moneda:moneda,usuario:usuario};

            vm.dataLoading = true
            UserService.DetalleContrato(moneda)
            .then(function(response) {
                vm.detallecontrato = response;
            });
        };

        function SetearNombreDetail(){
            var moneda = {moneda:$stateParams.moneda };
            var recibido = 'Recibidos';
            var realizado = 'Realizados';

            $cookies.put('recibidos', recibido);
            $cookies.put('realizados', realizado);
            
            if (moneda.moneda == 1) {
                var recibido = $cookies.get('recibidos');
                vm.contratos=recibido;    
            }else if (moneda.moneda == 2){
                var recibido = $cookies.get('recibidos');
                vm.contratos=recibido; 
            }
            else{
                var realizados = $cookies.get('realizados');
                vm.contratos=realizados; 
            }             
        }
   
    }

})();
