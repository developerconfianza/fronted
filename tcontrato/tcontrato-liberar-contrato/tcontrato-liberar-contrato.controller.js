(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoLiberarContratoController', TcontratoLiberarContratoController);

    TcontratoLiberarContratoController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TcontratoLiberarContratoController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadConfirmarContratoDetalle() ;
        

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarContratoDetalle() {

            UserService.LiberarContrato(vm.user.idUsuario)
            .then(function (response) {
              
                vm.liberarcontratodetail=response;


            });
        }
      
    }

})();
