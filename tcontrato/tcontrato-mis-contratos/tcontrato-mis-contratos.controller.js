(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratomisContratoController', TcontratomisContratoController);

    TcontratomisContratoController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TcontratomisContratoController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadMisContratos();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMisContratos() {

            UserService.Resumencontratorecibidosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.contratorecsoles=response;
            });

            UserService.Resumencontratorecibidodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.contratorecdolar=response;
            });

            UserService.Resumencontratorealizadosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.contratoreasoles=response;
            });

            UserService.Resumencontratorealizadodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.contratoreadolar=response;
            });
        
        } 
    }

})();
