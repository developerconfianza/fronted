(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoRegisterController', TcontratoRegisterController);

    TcontratoRegisterController.$inject = ['$state','$scope','UserService','$mdToast','$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TcontratoRegisterController($state,$scope,UserService,$mdToast,$location, $rootScope, FlashService,msgflash,$cookies,$q) {

        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("tcontrato");  
        vm.querySearch = querySearch;
        vm.contrato={contratante:{}}; 
        vm.registrarContrato = registrarContrato;
        vm.showSimpleToastDni=showSimpleToastDni;
        vm.pendiente=pendiente;
        vm.ObtenerDataUsuarioporDniContrato = ObtenerDataUsuarioporDniContrato;
        
        loadCurrentUser();
        loadData();
    
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function registrarContrato() {
            vm.progress = true;
            if (vm.contrato.contratado != null) {
                 $cookies.putObject('contrato', vm.contrato);
                var favoriteCookie = $cookies.getObject('contrato');
                $state.go('tcontrato/tcontrato-register.detail');
                vm.progress = false;
            }else{
                showSimpleToastDni();
                msgflash.error('Debe de seleccionar un Contratado', false);
            }
            
        }

         function ObtenerDataUsuarioporDniContrato($event) {
            msgflash.clear();
            if (vm.contrato.contratado != null) {
                $scope.visible = true;
                vm.progress = true;
                UserService.ObtenerDataUsuarioporDniContrato(vm.contrato.contratado.idUsuario)
                .then(function (response) {
                    vm.contrato.contratado=response.contratado;
                    ObtenerContratoUsuario();
                    vm.progress = false;
                });
            }else{
                $scope.visible = false;
                $event.stopPropagation();
                showSimpleToastDni();
                msgflash.error('Debe de seleccionar un contratado', false);

            }
            
        }

        function ObtenerContratoUsuario (){
            UserService.ObtenerContratosUsuario(vm.contrato.contratado.idUsuario)
            .then(function (response) {
                vm.contratousuario= response;
                
            });
        }

       //Load data default
        function loadData() {

            UserService.ObtenerServicios()
            .then(function (response) {
                vm.servicios_contrato = response;
            });
            
            vm.moneda = [
                {codigo: 'S', descripcion: 'Soles'},
                {codigo: 'D', descripcion: 'Dólares'}
            ]

            if (obtenernombre == 'tcontrato') {
                var contrato = $cookies.getObject('contrato');
                vm.contrato=contrato;   
            }

            vm.contrato.contratante=vm.user;


        }

         vm.sync_method;
        function querySearch(query) {

              UserService.DatosBusqueda(query)
             .then(function (response) {

                vm.sync_method.resolve(response);
            });

              vm.sync_method=$q.defer();
             return vm.sync_method.promise;
             
        }

        function showSimpleToastDni() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Debe de seleccionar un contratado')
            .position(pinTo )
            .hideDelay(3000)
            );
        }

        function pendiente(){
            var pendiente = vm.contrato.monto - vm.contrato.adelanto;
            vm.contrato.pendiente = pendiente;

        }

  
    }

})();
