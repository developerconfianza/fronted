(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoRegisterDetailController', TcontratoRegisterDetailController);

    TcontratoRegisterDetailController.$inject = ['$state','$http','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope','$mdDialog'];
    function TcontratoRegisterDetailController($state,$http,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope,$mdDialog) {
        var vm = this;

        vm.GuardarContrato = GuardarContrato;
        loadprestamo();
        
        function loadprestamo(){
            vm.user = $rootScope.globals.user;
            var contrato = $cookies.getObject('contrato');
            vm.contrato=contrato;
            var actual = new Date();
            vm.fecha=(actual.getDate() + "/" + (actual.getMonth() +1) + "/" + actual.getFullYear());
        }

        msgflash.clear();
        function GuardarContrato(){
            msgflash.clear();

            vm.dataLoading = true;

            UserService.RegistrarContrato(vm.contrato)
            .then(function(response){
                 msgflash.clear();

                $state.go('tcontrato/tcontrato-register');
                $cookies.remove('contrato');
                msgflash.success('Se guardo su contrato');  
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        GuardarContrato();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };
        
            
          
    }

})();
