(function () {
    'use strict';

    angular
        .module('app')
        .controller('TcontratoLiberarContratoDetailController', TcontratoLiberarContratoDetailController);

    TcontratoLiberarContratoDetailController.$inject = ['$stateParams','$state','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog','$scope'];
    function TcontratoLiberarContratoDetailController($stateParams,$state,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog,$scope) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadLiberarContratoDetalle() ;
        
        vm.LevantarContrato = LevantarContrato;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadLiberarContratoDetalle() {

            UserService.LiberardetalleContrato(id)
            .then(function (response) {
                vm.liberarcontrato=response.liberado;
            });
        }

        msgflash.clear();
        function LevantarContrato(){
            vm.dataLoading = true;
            var observacion;
            observacion="";
            if(vm.liberar)
            observacion= vm.liberar.observacion;
            var levantar = {idContrato:id,observacion:observacion};

            UserService.ActualizarLiberardeContrato(levantar)
            .then(function (response) {
                $state.go('tcontrato/tcontrato-liberar-contrato');
                msgflash.success('Se levanto el contrato');

                  
            });
        }

        $scope.showConfirm = function(ev) {
            $scope.status = '  ';
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
            LevantarContrato();
            },
            function() {
            $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
            });
      };
      
    }

})();
