(function () {
    'use strict';

    angular
        .module('app')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function DashboardController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        //console.log('usaurio actual');
        //console.log($rootScope.globals);

        initController();

        function initController() {
            loadCurrentUser();
        }

        function loadCurrentUser() {
            
            vm.user = $rootScope.globals.user;
       
        }
        
      
    }

})();
