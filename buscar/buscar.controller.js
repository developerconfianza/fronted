(function () {
    'use strict';

    angular
        .module('app')
        .controller('BuscarController', BuscarController);

    BuscarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function BuscarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

    var vm = this;
    vm.simulateQuery = false;
    vm.states        = loadAll();
    vm.querySearch   = querySearch;
  
    function querySearch (query) {
        return vm.states;
      /*var results = query ? vm.states.filter( createFilterFor(query) ) : vm.states,
          deferred;

       console.log(query);

      if (vm.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }*/
    }
  
    function loadAll() {
      var allStates = 'Aguascalientes,  Baja California, Coahuila, Oaxaca,\
              Puebla, Guerrero, Chiapas';
      return allStates.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });


    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }
      
    }

})();
