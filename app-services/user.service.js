﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};
    
        service.Autenticar = Autenticar;
        service.GuardarUsuario = GuardarUsuario;
        service.DatosBusqueda = DatosBusqueda;
        service.BuscarBenificiario = BuscarBenificiario;
        service.BuscarPrestamista = BuscarPrestamista;
        service.BuscarArrendatario = BuscarArrendatario;
        service.BuscarArrendador = BuscarArrendador;
        service.VerificarCorreo = VerificarCorreo;
        service.VerificarToken = VerificarToken;
        service.CambiarPasswordporToken = CambiarPasswordporToken;
        service.VerificarTokenCuenta = VerificarTokenCuenta;
        service.CambiarEstadoporToken = CambiarEstadoporToken;
        service.Editarusuario = Editarusuario;
        service.ObtenerDepartamentos = ObtenerDepartamentos;
        service.ObtenerProvincias =  ObtenerProvincias;
        service.ObtenerDistritos =  ObtenerDistritos;
        service.ActualizarUsuario =  ActualizarUsuario;
        service.EnviaridUsuario =  EnviaridUsuario;
        service.ObtenerPeriodicidad =  ObtenerPeriodicidad;
        service.Mired =  Mired;
        service.Favoritos =  Favoritos;
        service.ObtenerDatosporDniIngresado =  ObtenerDatosporDniIngresado;

        

        

        //tpresto
        service.RegistrarPrestamo =  RegistrarPrestamo;
        service.ObtenerDataUsuarioporDni =  ObtenerDataUsuarioporDni;
        service.ObtenerPrestamosUsuario =  ObtenerPrestamosUsuario;
        service.Resumenprestamorecibidosoles =  Resumenprestamorecibidosoles;
        service.Resumenprestamorecibidodolares =  Resumenprestamorecibidodolares;
        service.Resumenprestamorealizadosoles =  Resumenprestamorealizadosoles;
        service.Resumenprestamorealizadodolares =  Resumenprestamorealizadodolares;
        service.DetallePrestamo = DetallePrestamo;
        service.ConfirmarPrestamo = ConfirmarPrestamo;
        service.ConfirmarPrestamoDetalle =  ConfirmarPrestamoDetalle;
        service.RechazardetalleContrato =  RechazardetalleContrato;
        service.ConfirmardetalleContrato =  ConfirmardetalleContrato;
        service.LiberarPrestamo =  LiberarPrestamo;
        service.LiberardetallePrestamo =  LiberardetallePrestamo;
        service.ActualizarLiberarPrestamo =  ActualizarLiberarPrestamo;
        service.Obtenerdatospordnicuotas =  Obtenerdatospordnicuotas;
        service.Obtenercuotas =  Obtenercuotas;
        service.Obtenerdetallecuota =  Obtenerdetallecuota;
        service.ActualizarLiquidarCuota =  ActualizarLiquidarCuota;
        service.Obtenerdatospordnipagos =  Obtenerdatospordnipagos;
        service.Obtenercuotaspago =  Obtenercuotaspago;
        service.NumeroCuota =  NumeroCuota;
        service.PagarCuota =  PagarCuota;
        //tcontrato
        service.Resumencontratorecibidosoles =  Resumencontratorecibidosoles;
        service.Resumencontratorecibidodolares =  Resumencontratorecibidodolares;
        service.Resumencontratorealizadosoles =  Resumencontratorealizadosoles;
        service.Resumencontratorealizadodolares =  Resumencontratorealizadodolares;
        service.DetalleContrato =  DetalleContrato;
        service.ObtenerServicios =  ObtenerServicios;
        service.ObtenerDataUsuarioporDniContrato =  ObtenerDataUsuarioporDniContrato;
        service.ObtenerContratosUsuario =  ObtenerContratosUsuario;
        service.RegistrarContrato =  RegistrarContrato;
        service.ConfirmarContrato = ConfirmarContrato;
        service.ConfirmarContratoDetalle=ConfirmarContratoDetalle;
        service.RechazardetalledeContrato=RechazardetalledeContrato;
        service.ConfirmardetalledeContrato=ConfirmardetalledeContrato;
        service.LiberarContrato=LiberarContrato;
        service.LiberardetalleContrato=LiberardetalleContrato;
        service.ActualizarLiberardeContrato=ActualizarLiberardeContrato;
        //tvendo
        service.Resumenventarecibidosoles =  Resumenventarecibidosoles;
        service.Resumenventarecibidodolares =  Resumenventarecibidodolares;
        service.Resumenventarealizadosoles =  Resumenventarealizadosoles;
        service.Resumenventarealizadodolares =  Resumenventarealizadodolares;
        service.DetalleVenta =  DetalleVenta;
        service.ObtenerProductos =  ObtenerProductos;
        service.ObtenerDataUsuarioporDniVenta =  ObtenerDataUsuarioporDniVenta;
        service.ObtenerVentasUsuario =  ObtenerVentasUsuario;
        service.RegistrarVenta =  RegistrarVenta;
        service.ConfirmarVenta =  ConfirmarVenta;
        service.ConfirmarVentaDetalle =  ConfirmarVentaDetalle;
        service.RechazardetalledeVenta =  RechazardetalledeVenta;
        service.ConfirmardetalledeVenta =  ConfirmardetalledeVenta;
        service.LiberarVenta =  LiberarVenta;
        service.LiberardetalleVenta =  LiberardetalleVenta;
        service.ActualizarLiberardeVenta =  ActualizarLiberardeVenta;
        //talquilo
        service.Resumenalquilerrecibidosoles =  Resumenalquilerrecibidosoles;
        service.Resumenalquilerrecibidodolares =  Resumenalquilerrecibidodolares;
        service.Resumenalquilerrealizadosoles =  Resumenalquilerrealizadosoles;
        service.Resumenalquilerrealizadodolares =  Resumenalquilerrealizadodolares;
        service.DetalleAlquiler =  DetalleAlquiler;
        service.ObtenerDataUsuarioporDniAlquiler =  ObtenerDataUsuarioporDniAlquiler;
        service.ObtenerAlquilerUsuario =  ObtenerAlquilerUsuario;
        service.RegistrarAlquiler =  RegistrarAlquiler;
        service.ConfirmarAlquiler =  ConfirmarAlquiler;
        service.ConfirmarAlquilerDetalle =  ConfirmarAlquilerDetalle;
        service.ConfirmardetalledeAlquiler =  ConfirmardetalledeAlquiler;
        service.RechazardetalledeAlquiler =  RechazardetalledeAlquiler;
        service.LiberarAlquiler =  LiberarAlquiler;
        service.LiberardetalleAlquiler =  LiberardetalleAlquiler;
        service.ActualizarLiberardeAlquiler =  ActualizarLiberardeAlquiler;
        service.Obtenerdatospordnialquilerr =  Obtenerdatospordnialquilerr;
        service.Obteneralquiler =  Obteneralquiler;
        service.Obtenerdetallealquiler =  Obtenerdetallealquiler;
        service.ActualizarLiquidarAlquiler =  ActualizarLiquidarAlquiler;
        service.Obtenerdatospordnialquileres =  Obtenerdatospordnialquileres;
        service.Obtenerpagosalquiler =  Obtenerpagosalquiler;
        service.PagarAlquiler =  PagarAlquiler;

        return service;

        

      
        function Autenticar(email,password) {
            return $http.get('http://laravel42.prj17001.syslacsdev.com/usuarios/autenticarusuario/'+email+'/'+password)
            .then(handleSuccess, handleError('Error getting user by email'));
        }

        function GuardarUsuario(user) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/guardarusuario', user)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }

        
        function DatosBusqueda(palabra) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/datosbusqueda', palabra)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }

        function BuscarBenificiario(data) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/buscarbenificiario', data)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }
        function BuscarPrestamista(data) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/buscarprestamista', data)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }
        function BuscarArrendatario(data) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/buscararrendatario', data)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }
        function BuscarArrendador(data) {
            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/buscararrendador', data)
           .then(handleSuccess, handleError('Error getting user by username'));                  
        }


         function VerificarCorreo(user) {


            return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/verificarcorreo', user)
            .then(handleSuccess, handleError('email no encontrado'));
        }



        function VerificarToken(token){
        return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/verificartoken',token)
            .then(handleSuccess, handleError('error'));
        }


        function EnviaridUsuario(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/recibirid',idusuario)
          .then(handleSuccess, handleError('error'));
        }


        function CambiarPasswordporToken(token){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarpasswordtoken',token)
          .then(handleSuccess, handleError('error'));
        }

          function VerificarTokenCuenta(token){
        return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/verificartokencuenta',token)
            .then(handleSuccess, handleError('error'));
        }
        function CambiarEstadoporToken(token){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarestadotoken',token)
          .then(handleSuccess, handleError('error'));
        }


        function Editarusuario(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/editarusuario',user)
          .then(handleSuccess, handleError('error'));
        }

        function Mired(iduser){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/mired',iduser)
          .then(handleSuccess, handleError('error'));
        }

        function Favoritos(iduser){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/favoritos',iduser)
          .then(handleSuccess, handleError('error'));
        }

        function ObtenerPeriodicidad(){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listarperiodicidad')
          .then(handleSuccess, handleError('error'));
        }

        function ObtenerDepartamentos(){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listardepartamentos')
          .then(handleSuccess, handleError('error'));
        }

        function ObtenerProvincias(iddepa){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listarprovincias',iddepa)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }

        function ObtenerDistritos(idprov){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listardistritos',idprov)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }

        function ActualizarUsuario(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarusuario',user)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ObtenerDatosporDniIngresado(dni){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordniingresado',dni)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

         

        //tpresto

        function RegistrarPrestamo(prestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/guardarprestamo',prestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  

        function ObtenerDataUsuarioporDni(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordni',user)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }

        function ObtenerPrestamosUsuario(prestamos){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerprestamosusuario',prestamos)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenprestamorecibidosoles(prestamorecsoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenprestamorecibidosoles',prestamorecsoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenprestamorecibidodolares(prestamorecdolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenprestamorecibidodolar',prestamorecdolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenprestamorealizadosoles(prestamoreasoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenprestamorealizadosoles',prestamoreasoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function Resumenprestamorealizadodolares(prestamoreadolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenprestamorealizadodolar',prestamoreadolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  


        function DetallePrestamo(moneda){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/detalleprestamo',moneda)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmarPrestamo(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarprestamo ',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function ConfirmarPrestamoDetalle(confirmardetalle){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarprestamodetalle ',confirmardetalle)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmardetalleContrato(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarestadocontrato ',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function RechazardetalleContrato(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/rechazarestadocontrato ',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }

         

        function LiberarPrestamo(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarprestamo',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberardetallePrestamo(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarprestamodetalle ',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiberarPrestamo(prestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliberarcontrato',prestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  

        function Obtenerdatospordnicuotas(dni){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnicuotas',dni)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obtenercuotas(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenercuotas',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Obtenerdetallecuota(idcuota){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listardetalleliquidarcuota',idcuota)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiquidarCuota(liquidar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliquidarcuota',liquidar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obtenerdatospordnipagos(dni){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnipagos',dni)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obtenercuotaspago(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenercuotasdepago',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function NumeroCuota(idcuota){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/numerocuota',idcuota)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }

        function PagarCuota(pago){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/pagarcuota',pago)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        //tcontrato
        function Resumencontratorecibidosoles(contratorecsoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumencontratorecibidosoles',contratorecsoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumencontratorecibidodolares(contratorecdolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumencontratorecibidodolar',contratorecdolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumencontratorealizadosoles(contratoreasoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumencontratorealizadosoles',contratoreasoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function Resumencontratorealizadodolares(contratoreadolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumencontratorealizadodolar',contratoreadolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function DetalleContrato(moneda){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/detallecontrato',moneda)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  
        function ObtenerServicios(){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listarservicios')
          .then(handleSuccess, handleError('error'));
        }
        function ObtenerDataUsuarioporDniContrato(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnicontrato',user)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function ObtenerContratosUsuario(contratos){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenercontratosusuario',contratos)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function RegistrarContrato(contrato){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/guardarcontrato',contrato)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  

        function ConfirmarContrato(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarcontrato ',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmarContratoDetalle(confirmardetalle){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarcontratodetalle ',confirmardetalle)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmardetalledeContrato(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarestadodecontrato ',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function RechazardetalledeContrato(idprestamo){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/rechazarestadodecontrato ',idprestamo)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberarContrato(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarcontrato',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberardetalleContrato(idcontrato){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarcontratodetalle ',idcontrato)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiberardeContrato(contrato){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliberardecontrato',contrato)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  

        //tvendo
        function Resumenventarecibidosoles(ventarecsoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenventarecibidosoles',ventarecsoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenventarecibidodolares(ventarecdolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenventarecibidodolar',ventarecdolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenventarealizadosoles(ventareasoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenventarealizadosoles',ventareasoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function Resumenventarealizadodolares(ventareadolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenventarealizadodolar',ventareadolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function DetalleVenta(moneda){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/detalleventa',moneda)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ObtenerProductos(){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listarproductos')
          .then(handleSuccess, handleError('error'));
        } 

        function ObtenerDataUsuarioporDniVenta(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordniventa',user)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function ObtenerVentasUsuario(ventas){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerventasusuario',ventas)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function RegistrarVenta(venta){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/guardarventa',venta)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmarVenta(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarventa ',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ConfirmarVentaDetalle(confirmardetalle){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmarventadetalle ',confirmardetalle)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function ConfirmardetalledeVenta(idventa){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarestadodeventa ',idventa)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function RechazardetalledeVenta(idventa){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/rechazarestadodeventa ',idventa)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberarVenta(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarventa',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberardetalleVenta(idventa){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberarventadetalle ',idventa)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiberardeVenta(venta){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliberardeventa',venta)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  

        //talquilo
        function Resumenalquilerrecibidosoles(alquilerrecsoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenalquilerrecibidosoles',alquilerrecsoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenalquilerrecibidodolares(alquilerrecdolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenalquilerrecibidodolar',alquilerrecdolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function Resumenalquilerrealizadosoles(alquilerreasoles){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenalquilerrealizadosoles',alquilerreasoles)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function Resumenalquilerrealizadodolares(alquilerreadolar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/resumenalquilerrealizadodolar',alquilerreadolar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  
        function DetalleAlquiler(moneda){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/detallealquiler',moneda)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function ObtenerDataUsuarioporDniAlquiler(user){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnialquiler',user)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ObtenerAlquilerUsuario(alquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obteneralquilerusuario',alquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));

        }

        function RegistrarAlquiler(alquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/guardaralquiler',alquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

          function ConfirmarAlquiler(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmaralquiler ',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }
        function ConfirmarAlquilerDetalle(confirmardetalle){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/confirmaralquilerdetalle ',confirmardetalle)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function ConfirmardetalledeAlquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarestadodealquiler ',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 
        function RechazardetalledeAlquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/rechazarestadodealquiler ',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberarAlquiler(idusuario){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberaralquiler',idusuario)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function LiberardetalleAlquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/liberaralquilerdetalle ',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiberardeAlquiler(alquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliberardealquiler',alquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obtenerdatospordnialquilerr(dni){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnialquilerr',dni)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obteneralquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obteneralquiler',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }  
         

        function Obtenerdetallealquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/listardetalleliquidaralquiler',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function ActualizarLiquidarAlquiler(liquidar){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/actualizarliquidaralquiler',liquidar)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

         function Obtenerdatospordnialquileres(dni){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerdatospordnialquileres',dni)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function Obtenerpagosalquiler(idalquiler){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/obtenerpagosalquiler',idalquiler)
          .then(handleSuccess, handleError('error en obtener provincia'));
        } 

        function PagarAlquiler(pago){

          return $http.post('http://laravel42.prj17001.syslacsdev.com/usuarios/pagaralquiler',pago)
          .then(handleSuccess, handleError('error en obtener provincia'));
        }




        // private functions

        function handleSuccess(res) {
            console.log("respuessta del web services ");
            console.log(res);
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }




    }

})();
