(function () {
    'use strict';

    angular
        .module('app')
        .controller('buscarController', buscarController);

        
    buscarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash'];
    function buscarController(UserService, $location, $rootScope, FlashService,msgflash) {
       

         var vm = this;
    vm.simulateQuery = false;
    // list of `state` value/display objects
    vm.states        = loadAll();
    vm.querySearch   = querySearch;
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? vm.states.filter( createFilterFor(query) ) : vm.states,
          deferred;
      if (vm.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }
    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
      var allStates = 'Aguascalientes,  Baja California, Coahuila, Oaxaca,\
              Puebla, Guerrero, Chiapas';
      return allStates.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };
    }
    }
     
})();
