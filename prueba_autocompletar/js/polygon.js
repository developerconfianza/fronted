/*
*
Alerts
*
*/

$('.missing').click(function() {
    //console.log($(this).parent());
    $(this).parent().fadeOut();
});

/*
*
Tooltip
*
*/
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

/*
*
Popover
*
*/
$(function () {
  $('[data-toggle="popover"]').popover()
})

/*
Button collapse animation
*/
$(document).ready(function(){
	$('#btn-collapse').click(function() {
	    //$('#menu-collapse').slideToggle();
	    $('#menu-collapse').toggleClass('active');
	});
});


