(function () {
    'use strict';
      //var initialized = false;
    angular
        .module('app', ['ngRoute', 'ngCookies', 'kinvey',
      'ngMessages',
      'ngMaterial',
      'ngAnimate',
      'ngAria',
      'ngResource',
      'ngStorage',
      'ngMask',
      'ui.bootstrap',
      'blockUI'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider

            .when('/buscar', {
                controller: 'buscarController',
                templateUrl: 'buscar/buscar.view.html',
                controllerAs: 'vm'
            })
            .otherwise({ redirectTo: '/buscar' });
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
    function run($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to buscar page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/buscar'/*,'/habilitar-cuenta',*/]) === -1;
        
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/buscar');
            }
        });
    }

   
})();