(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoRegisterDetailController', TprestoRegisterDetailController);

    TprestoRegisterDetailController.$inject = ['$state','$http','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope','$mdDialog'];
    function TprestoRegisterDetailController($state,$http,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope,$mdDialog) {
        var vm = this;

        vm.GuardarPrestamo = GuardarPrestamo    
        loadprestamo();    
        $scope.cuotas = [];
      
        function Agregarcuota() {
            for(var i=0;i<vm.prestamo.nro_cuotas;i++) {
                $scope.cuotas.push(
                 {nro_cuota:i+1, monto: vm.prestamo.monto/vm.prestamo.nro_cuotas, capital:vm.prestamo.monto,interes:vm.prestamo.tasa/vm.prestamo.nro_cuotas, fechapago:vm.prestamo.fecha_pago}
                );
            }   
        }

        Agregarcuota();

        function loadprestamo(){

            vm.user = $rootScope.globals.user;
            var prestamo = $cookies.getObject('prestamo');
            vm.prestamo=prestamo;
            var actual = new Date();
            vm.fecha=(actual.getDate() + "/" + (actual.getMonth() +1) + "/" + actual.getFullYear());
        }

        
        msgflash.clear();
        function GuardarPrestamo(){

            msgflash.clear();
            vm.dataLoading = true;
            UserService.RegistrarPrestamo(vm.prestamo)
            .then(function(response){
                msgflash.clear();
                $state.go('tpresto/tpresto-register');
                $cookies.remove('prestamo');
                msgflash.success('Se guardo su contrato');   
            });
        }
       
        $scope.showConfirm = function(ev) {

            $scope.status = '';
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
            GuardarPrestamo();
            },

            function() {
                $scope.status = 'No se realizo la acción.';
            });
        };
                 
    }

})();
