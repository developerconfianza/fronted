(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoliquidarCuentasController', TprestoliquidarCuentasController);

    TprestoliquidarCuentasController.$inject = ['$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TprestoliquidarCuentasController($scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$q) {
        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("liquidarcuotas");
        vm.querySearch   = querySearch;
        vm.ObtenerDataNombre = ObtenerDataNombre;
        loadCurrentUser();
        Loadcuotas();

        function loadCurrentUser() {

            vm.user = $rootScope.globals.user;
        }

        function ObtenerDataNombre() {

            var nombrebeneficiario = vm.selectedItem;
            if (nombrebeneficiario != null) {

                var idprestamo= vm.selectedItem.idPrestamo;
                var usuario= vm.user.idUsuario;
                var liquidar = {dniprestamista:usuario,idbeneficiario:vm.selectedItem.idUsuario,idprestamo:idprestamo};
                UserService.Obtenerdatospordnicuotas(liquidar)
                .then(function (response) {
                $cookies.put('nombre', response.nombrecuotas.idPrestamo);
                $cookies.put('correobeneficiario', response.nombrecuotas.correoUsuario);
                $cookies.put('nombrebeneficiario', vm.selectedItem.nombresUsuarios);
                Obtenercuotas();
                });
         
            }

        }
              
        $scope.visible=false;

        function Obtenercuotas(){
            $scope.visible=true;
            var idprestamo= $cookies.get('nombre');
            UserService.Obtenercuotas(idprestamo)
            .then(function (response) {
                vm.detallecuotas=response;
                $cookies.putObject('liquidarcuotas', response);
            });

        }

        function Loadcuotas(){
            if (obtenernombre == 'liquidarcuotas') {
                var prestamo = $cookies.getObject('liquidarcuotas');
                var nombrebeneficiario = $cookies.get('nombrebeneficiario');
                vm.detallecuotas=prestamo;
                vm.selectedItem=nombrebeneficiario;
            }
        }

        vm.sync_method;
        function querySearch(query) {
              var idUsuario= vm.user.idUsuario;
              var data= {palabra:query,idusuario:idUsuario};
              UserService.BuscarBenificiario(data)
             .then(function (response) {
                vm.sync_method.resolve(response);
            });

            vm.sync_method=$q.defer();
            return vm.sync_method.promise;  
        }
    }

})();
