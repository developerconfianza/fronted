(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoRegisterController', TprestoRegisterController);

    TprestoRegisterController.$inject = ['$state','UserService','$mdToast', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$q','$scope'];
    function TprestoRegisterController($state,UserService,$mdToast, $location, $rootScope, FlashService,msgflash,$cookies,$q,$scope) {

        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("tpresto");  
        vm.querySearch = querySearch;
        vm.prestamo={prestatario:{}};
        vm.registrarPrestamo = registrarPrestamo;
        vm.showSimpleToast=showSimpleToast;
        vm.ObtenerDataUsuarioporDni = ObtenerDataUsuarioporDni;
        
        loadCurrentUser();
        loadData();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            vm.prestamo.checkinteres= false;
        }

        msgflash.clear();

        function registrarPrestamo() {

            vm.progress = true;
            if (vm.prestamo.beneficiario != null) { 
                $cookies.putObject('prestamo', vm.prestamo);
                console.log('aqui todo el prestamo');
                console.log(vm.prestamo);
                $state.go('tpresto/tpresto-register.detail');
                vm.progress = false;
                 
            }else{
                showSimpleToast();
                msgflash.error('Debe de seleccionar un Beneficiario', false);  
            } 
        }


        function ObtenerDataUsuarioporDni($event) {

            msgflash.clear();
            if (vm.prestamo.beneficiario != null) {
                $scope.visible = true;
                vm.progress = true;
                UserService.ObtenerDataUsuarioporDni(vm.prestamo.beneficiario.idUsuario)
                .then(function (response) {
                    vm.prestamo.beneficiario=response.beneficiario;
                    ObtenerPrestamoUsuario();
                    vm.progress = false;
                });
            }else{
                $scope.visible = false;
                $event.stopPropagation();
                showSimpleToast();
                msgflash.error('Debe de seleccionar un Beneficiario', false);
            } 
        }

        function ObtenerPrestamoUsuario (){
            UserService.ObtenerPrestamosUsuario(vm.prestamo.beneficiario.idUsuario)
            .then(function (response) {
                vm.prestamousuario= response;
            });
        }

        function loadData() {

            UserService.ObtenerPeriodicidad()
            .then(function (response) {
                vm.periodo_cuotas = response;
            });
            
            vm.moneda = [
                {codigo: 'S', descripcion: 'Soles'},
                {codigo: 'D', descripcion: 'Dólares'}
            ]
            vm.periodo_tasa = [
                {codigo: '1', descripcion: 'Mensual'},
                {codigo: '2', descripcion: 'Anual'}
            ]

            if (obtenernombre == 'tpresto') {
                var prestamo = $cookies.getObject('prestamo');
                vm.prestamo=prestamo;
            }

            vm.prestamo.prestatario=vm.user;
        }


        vm.sync_method;
        function querySearch(query) {

              UserService.DatosBusqueda(query)
             .then(function (response) {
                vm.sync_method.resolve(response);
            });

            vm.sync_method=$q.defer();
            return vm.sync_method.promise;
             
        }

        function showSimpleToast() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Debe de seleccionar un Beneficiario')
            .position(pinTo )
            .hideDelay(3000)
            );
        }
    }

})();
