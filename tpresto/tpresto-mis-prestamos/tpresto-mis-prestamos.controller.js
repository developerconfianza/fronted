(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestomisPrestamoController', TprestomisPrestamoController);

    TprestomisPrestamoController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TprestomisPrestamoController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadMisPrestamos();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMisPrestamos() {

            UserService.Resumenprestamorecibidosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.prestamorecsoles=response;
            });

            UserService.Resumenprestamorecibidodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.prestamorecdolar=response;
            });

            UserService.Resumenprestamorealizadosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.prestamoreasoles=response;
            });

            UserService.Resumenprestamorealizadodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.prestamoreadolar=response;
            });
        
        } 
    }

})();
