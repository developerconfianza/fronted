(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestopagarCuotasDetailController', TprestopagarCuotasDetailController);

    TprestopagarCuotasDetailController.$inject = ['$stateParams','$mdDialog','$scope','$state','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TprestopagarCuotasDetailController($stateParams,$mdDialog,$scope,$state,UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        
        vm.PagarCuota = PagarCuota;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            NumeroCuota();
        }

        function NumeroCuota(){
            UserService.NumeroCuota(id)
            .then(function (response) {
                $cookies.put('numerocuota', response.numeroCuota);          
            });
        }

        msgflash.clear();
        function PagarCuota(){
            vm.dataLoading = true;
            var pago= vm.pago;
            var nombre=vm.user.nombresUsuario;
            var apellido=vm.user.apellidosUsuario;
            var correoprestamista=$cookies.get('correoprestamista');
            var numerocuota=$cookies.get('numerocuota');
            var pagos = {idcuota:id,pago:pago,correo:correoprestamista,nombre:nombre,
                        apellido:apellido,numerocuota:numerocuota};

            UserService.PagarCuota(pagos)
            .then(function (response) {
                $state.go('tpresto/tpresto-pagar-cuotas');   
                msgflash.success('Se Pago la cuota');           
            });
        }

        $scope.showConfirm = function(ev) {

            $scope.status = '';
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
                PagarCuota();
            },
            function() {
                $scope.status = 'No se realizo la acción';
            });
        };
    }

})();