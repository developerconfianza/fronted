(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoliquidarCuentasDetailController', TprestoliquidarCuentasDetailController);

    TprestoliquidarCuentasDetailController.$inject = ['$stateParams','$state','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope','$mdDialog'];
    function TprestoliquidarCuentasDetailController($stateParams, $state,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope,$mdDialog) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadDetalleLiquidarCuota() ;
        
        vm.LiquidarCuota = LiquidarCuota;

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            NumeroCuota();
        }

        function NumeroCuota(){
            UserService.NumeroCuota(id)
            .then(function (response) {
                $cookies.put('numerocuotaliquidar', response.numeroCuota);          
            });
        }

        
        function loadDetalleLiquidarCuota() {

            UserService.Obtenerdetallecuota(id)
            .then(function (response) {
                vm.liquidarcuota=response.liquidar;
            });
        }

        msgflash.clear();
        function LiquidarCuota(){

            vm.dataLoading = true;
            var observacion;
            observacion="";
            if(vm.liquidar)
            observacion= vm.liquidar.observacion;
            var nombre= vm.user.nombresUsuario;
            var apellido= vm.user.apellidosUsuario;
            var correobeneficiario=$cookies.get('correobeneficiario');
            var numerocuota=$cookies.get('numerocuotaliquidar');
            var liquidar = {idcuota:id,observacion:observacion,apellido:apellido,
                            nombre:nombre,correo:correobeneficiario,numerocuota:numerocuota};

            UserService.ActualizarLiquidarCuota(liquidar)
            .then(function (response) {
                $state.go('tpresto/tpresto-liquidar-cuentas');  
                msgflash.success('Se liquido la cuota');           
            });
        }

        $scope.showConfirm = function(ev) {
            $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
            LiquidarCuota();
            },

            function() {
            $scope.status = 'No se realizo la acción';
            console.log($scope.status);
            });
        };
    }

})();