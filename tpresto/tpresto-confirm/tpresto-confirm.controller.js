(function () {
    'use strict';

    angular
        .module('app')
        .controller('TPrestoConfirmarController', TPrestoConfirmarController);

    TPrestoConfirmarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TPrestoConfirmarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadConfirmarPrestamo();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarPrestamo() {

            UserService.ConfirmarPrestamo(vm.user.idUsuario)
            .then(function (response) {
                vm.confirmarprestamo=response;
            });
        } 
    }

})();
