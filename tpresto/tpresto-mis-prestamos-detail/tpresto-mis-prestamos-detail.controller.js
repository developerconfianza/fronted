(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestomisPrestamoDetailController', TprestomisPrestamoDetailController);

    TprestomisPrestamoDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope'];
    function TprestomisPrestamoDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope) {
        var vm = this;

        var moneda = {moneda:$stateParams.moneda };
        loadCurrentUser();
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            vm.nombre = $rootScope.globals.recibido;
        }

        Cargardetalleprestamo();
        SetearNombreDetail(); 

        function Cargardetalleprestamo() {
            
            var moneda = {moneda:$stateParams.moneda };
            var usuario= vm.user.idUsuario;
            var moneda = {moneda:moneda,usuario:usuario};

            vm.dataLoading = true
            UserService.DetallePrestamo(moneda)
            .then(function(response) {
                vm.detalleprestamo = response;
            });
        };

        function SetearNombreDetail(){

            var recibido = 'Recibidos';
            var realizado = 'Realizados';
            $cookies.put('recibido', recibido);
            $cookies.put('realizado', realizado);
            
            if (moneda.moneda == 1) {
                var recibido = $cookies.get('recibido');
                vm.prestamos=recibido;    
            }else if (moneda.moneda == 2){
                var recibido = $cookies.get('recibido');
                vm.prestamos=recibido;

            }
            else{
                var realizado = $cookies.get('realizado');
                vm.prestamos=realizado;
            }             
        }
   
    }

})();
