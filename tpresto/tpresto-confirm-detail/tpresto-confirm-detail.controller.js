(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoConfirmarPrestamoController', TprestoConfirmarPrestamoController);

    TprestoConfirmarPrestamoController.$inject = ['$stateParams','$state','$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog'];
    function TprestoConfirmarPrestamoController($stateParams,$state,$scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog) {
        var vm = this;     
        loadCurrentUser();
        var id = {id:$stateParams.id };
        loadConfirmarPrestamoDetalle();
        vm.ConfirmardetalleContrato = ConfirmardetalleContrato;
        vm.RechazardetalleContrato=RechazardetalleContrato;
     
        function loadCurrentUser() {
        vm.user = $rootScope.globals.user;
        }

        function loadConfirmarPrestamoDetalle() {
          
          UserService.ConfirmarPrestamoDetalle(id)
            .then(function (response) {
                vm.confirmarprestamodetail=response.confirmado;
                $cookies.put('correo', vm.confirmarprestamodetail.correoUsuario);
                //creo un array para las cuotas
                $scope.cuotasdetail = [];
                for(var i=0;i<vm.confirmarprestamodetail.cuotasPrestamo;i++) {
                    $scope.cuotasdetail.push(
                    {nro_cuota:i+1, monto: vm.confirmarprestamodetail.montoPrestamo/vm.confirmarprestamodetail.cuotasPrestamo, capital:vm.confirmarprestamodetail.montoPrestamo,interes:vm.confirmarprestamodetail.tasaInteresPrestamo, fechapago:vm.confirmarprestamodetail.fechaPagoPrestamo}
                    );
                }
            });
        } 
        msgflash.clear();
        function ConfirmardetalleContrato(){
            vm.dataLoading = true;  
            UserService.ConfirmardetalleContrato(id)
            .then(function (response) {
                $state.go('tpresto/tpresto-confirm');   
                msgflash.success('Se confirmo el contrato');
                vm.dataLoading= false;
                  
            });
        }

        function RechazardetalleContrato(){

            vm.dataLoading = true;  
            var correo =  $cookies.get('correo');
            var nombre =  vm.user.nombresUsuario;
            var apellido =  vm.user.apellidosUsuario;
            var rechazo = {origen:id,correo:correo,nombre:nombre,apellido:apellido};
            UserService.RechazardetalleContrato(rechazo)
            .then(function (response) {
                $state.go('tpresto/tpresto-confirm');   
                msgflash.success('Se rechazo el contrato');
                vm.dataLoading= false;
                  
            });
        }

        $scope.showConfirm = function(ev) {
          $scope.status = '  ';
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

          $mdDialog.show(confirm)
          .then(function() {
          ConfirmardetalleContrato();
          },
          function() {
          $scope.status = 'No se realizo la acción';
          });
        };

      $scope.showRechazar = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        RechazardetalleContrato();
        },
        function() {
          $scope.status = 'No se realizo la acción';
           console.log($scope.status);
        });
      };
    }

})();
