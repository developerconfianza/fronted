(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoLiberarPrestamoDetailController', TprestoLiberarPrestamoDetailController);

    TprestoLiberarPrestamoDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog','$scope'];
    function TprestoLiberarPrestamoDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog,$scope) {
        var vm = this;
        var id = {id:$stateParams.id };

        vm.LevantarPrestamo = LevantarPrestamo; 
        loadCurrentUser();
        loadLiberarPrestamoDetalle() ;
        
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        
        function loadLiberarPrestamoDetalle() {

            UserService.LiberardetallePrestamo(id)
            .then(function (response) {
                vm.liberarprestamo=response.liberado;
            });
        }

        msgflash.clear();
        function LevantarPrestamo(){

            vm.dataLoading = true;   
            var observacion;
            observacion="";
            if(vm.liberar)
            observacion= vm.liberar.observacion;
            var levantar = {idprestamo:id,observacion:observacion};
            UserService.ActualizarLiberarPrestamo(levantar)
            .then(function (response) {
            $location.path('/tpresto/tpresto-liberar-prestamo');
            msgflash.success('Se levanto el prestamo');                
            });            
        }

        $scope.showConfirm = function(ev) {

            $scope.status = '';
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
            LevantarPrestamo();
            },
            function() {
                $scope.status = 'No se realizo la acción.';
            });
      };
    }

})();
