(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestopagarCuotasController', TprestopagarCuotasController);

    TprestopagarCuotasController.$inject = ['$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TprestopagarCuotasController($scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$q) {
        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("pagarcuotas");
        vm.querySearch   = querySearch;
        vm.ObtenerDataDni = ObtenerDataDni;
        loadCurrentUser();
        Loadcuotas();


        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function ObtenerDataDni() {
            var nombreprestamista = vm.pagar;
            var idprestamo = vm.pagar;

            if (nombreprestamista != null) {
            var usuario= vm.user.idUsuario;
            var pago = {nombreprestamista:vm.pagar.idUsuario,idbeneficiario:usuario,idprestamo:vm.pagar.idPrestamo};
            UserService.Obtenerdatospordnipagos(pago)
            .then(function (response) {
                $cookies.put('datauser', response.pagarcuotas.idPrestamo);
                $cookies.put('correoprestamista', response.pagarcuotas.correoUsuario);
                $cookies.put('nombreprestamista', vm.pagar.nombresUsuarios);
                
                Obtenercuotaspago();
            });

            }
            
        }

        $scope.visible=false;
        function Obtenercuotaspago(){

            $scope.visible=true;
            var idprestamo= $cookies.get('datauser');
            UserService.Obtenercuotaspago(idprestamo)
            .then(function (response) {
                vm.detallecuotas=response;
                $cookies.putObject('pagarcuotas', response);
            });

        }

        function Loadcuotas(){
            if (obtenernombre == 'pagarcuotas') {
                var cuotas = $cookies.getObject('pagarcuotas');
                var nombreprestamista = $cookies.get('nombreprestamista');
                vm.detallecuotas=cuotas;
                vm.pagar=nombreprestamista;
            }
        }

        vm.sync_method;
        function querySearch(query) {

            var idUsuario= vm.user.idUsuario;
            var data= {palabra:query,idusuario:idUsuario};

            UserService.BuscarPrestamista(data)
            .then(function (response) {
                vm.sync_method.resolve(response);
            });

            vm.sync_method=$q.defer();
            return vm.sync_method.promise;
             
        }
    }

})();