(function () {
    'use strict';

    angular
        .module('app')
        .controller('TprestoLiberarPrestamoController', TprestoLiberarPrestamoController);

    TprestoLiberarPrestamoController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TprestoLiberarPrestamoController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadConfirmarPrestamoDetalle() ;
        

        function loadCurrentUser() {
            
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarPrestamoDetalle() {

            UserService.LiberarPrestamo( vm.user.idUsuario)
            .then(function (response) {
                vm.liberarprestamodetail=response;
            });
        }
      
    }

})();
