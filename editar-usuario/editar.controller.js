(function () {
    'use strict';

    angular
        .module('app')
        .controller('EditarController', EditarController);

        
    EditarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function EditarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {

      var vm = this;
      vm.user = null;
     
      //console.log('aqui va el editar');
      //console.log($rootScope.globals);

      vm.cambiarprovincias = cambiarprovincias;
      vm.cambiardistritos = cambiardistritos;
      initController();
      vm.Actualizar = Actualizar;

      function initController() {
        loadCurrentUser();     
      }

      function loadCurrentUser() {

        UserService.EnviaridUsuario($rootScope.globals.user.idUsuario)
        .then(function (response) {
          response.checkpassword = false;
          vm.user = response;
          inicializar();
        });       
      }

      function inicializar(){
        loadDepartamentos();
        cambiardistritos();
        cambiarprovincias();

      }


      function loadDepartamentos() {

        UserService.ObtenerDepartamentos()
        .then(function (response) {
          vm.departamentos = response;
        });


        vm.tipo_documento = [
          {codigo: '1', descripcion: 'DNI'},
          {codigo: '2', descripcion: 'Carnet de Extranjería'},
          {codigo: '3', descripcion: 'Otros'}
        ]

        vm.sexo = [
          {codigo: '2', descripcion: 'Femenino'},
          {codigo: '1', descripcion: 'Masculino'}
        ]

      }

      function cambiarprovincias(){

        UserService.ObtenerProvincias(vm.user.idDepartamento)
        .then(function (response) {
          vm.provincias = response;
        });
      }

      function cambiardistritos(){

        UserService.ObtenerDistritos(vm.user.idProvincia)
        .then(function (response) {
          vm.distritos = response;      
        });
      }

      msgflash.clear();
      function Actualizar() {

        vm.dataLoading = true;
        UserService.ActualizarUsuario(vm.user)
        .then(function (response) { 
          msgflash.clear();
          if(response.success) {

            $rootScope.globals.user = response.user;
            var cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 7);
            $cookies.putObject('globals', $rootScope.globals, { expires: cookieExp });

            msgflash.success('usuario actualizado', true);

            vm.user.checkpassword=''  ;
            vm.user.passwordUsuario=''  ;
            vm.user.passwordUsuarioconfirmar=''  ;                
          } else {
            msgflash.error(response.error, false);         
            vm.dataLading = false;
            
          }
        });
      }
  }
     

})();
