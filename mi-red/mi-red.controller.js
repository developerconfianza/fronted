(function () {
    'use strict';

    angular
        .module('app')
        .controller('MiredController', MiredController);

    MiredController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function MiredController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadMiRed();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMiRed() {

            UserService.Mired(vm.user.idUsuario)
            .then(function (response) {
                vm.mired=response;
            });
        } 
    }

})();
