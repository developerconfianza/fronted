(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloliquidarAlquilerDetailController', TalquiloliquidarAlquilerDetailController);

    TalquiloliquidarAlquilerDetailController.$inject = ['$stateParams','$scope','$state','$mdDialog','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TalquiloliquidarAlquilerDetailController($stateParams,$scope,$state,$mdDialog,UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadDetalleLiquidarAlquiler() ;
        
        vm.LiquidarAlquiler = LiquidarAlquiler;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        
        function loadDetalleLiquidarAlquiler() {

            UserService.Obtenerdetallealquiler(id)
            .then(function (response) {
                vm.liquidaralquiler=response.liquidar;
                console.log('response');
                console.log(response.liquidar);
            });
        }

        msgflash.clear();
        function LiquidarAlquiler(){
            vm.dataLoading = true;

            var observacion;
            observacion="";
            if(vm.liquidar)
             observacion= vm.liquidar.observacion;
            var nombre= vm.user.nombresUsuario;
            var apellido= vm.user.apellidosUsuario;
            var correobeneficiario=$cookies.get('correobeneficiario');
            var liquidar = {idalquiler:id,observacion:observacion,apellido:apellido,
                            nombre:nombre,correo:correobeneficiario};

            UserService.ActualizarLiquidarAlquiler(liquidar)
            .then(function (response) {
                $state.go('talquilo/talquilo-liquidar-alquiler'); 
                msgflash.success('Se liquido el alquiler');           
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        LiquidarAlquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción';
           console.log($scope.status);
        });
      };
    }

})();