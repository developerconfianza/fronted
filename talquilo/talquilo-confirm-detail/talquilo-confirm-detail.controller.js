(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloConfirmarAlquilerController', TalquiloConfirmarAlquilerController);

    TalquiloConfirmarAlquilerController.$inject = ['$stateParams','$state','$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog'];
    function TalquiloConfirmarAlquilerController($stateParams,$state,$scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog) {
        var vm = this;
        var id = {id:$stateParams.id };

        loadCurrentUser();
        loadConfirmarAlquilerDetalle();
        vm.Confirmardetalledealquiler = Confirmardetalledealquiler;
        vm.Rechazardetalledealquiler=Rechazardetalledealquiler;
     
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarAlquilerDetalle() {

            UserService.ConfirmarAlquilerDetalle(id)
            .then(function (response) {
                vm.confirmaralquilerdetail=response.confirmado;
                $cookies.put('correo', response.confirmado.correoUsuario);
               // $cookies.put('nombrearrendador', response.confirmado.nombresUsuario);
                //$cookies.put('apellidoarrendador', response.confirmado.apellidosUsuario);

            });
        } 
        msgflash.clear();
        function Confirmardetalledealquiler(){
            vm.dataLoading = true;  
           /* var correoarrendador =  $cookies.get('correoarrendador');
            var nombrearrendador =  $cookies.get('nombrearrendador');
            var apellidoarrendador =  $cookies.get('apellidoarrendador');
            var nombrearrendatario =  vm.user.nombresUsuario;
            var apellidoarrendatario =  vm.user.apellidosUsuario;
            var correoarrendatario=vm.user.correoUsuario;
            var confirmado = {origen:id,correoarrendador:correoarrendador,nombrearrendador:nombrearrendador,
              apellidoarrendador:apellidoarrendador,nombrearrendatario:nombrearrendatario,apellidoarrendatario:apellidoarrendatario,
              correoarrendatario:correoarrendatario};*/ 
            UserService.ConfirmardetalledeAlquiler(id)
            .then(function (response) {
                $state.go('talquilo/talquilo-confirm');
                msgflash.success('Se confirmo el alquiler');

                vm.dataLoading= false;
                  
            });
        }

        function Rechazardetalledealquiler(){
            vm.dataLoading = true; 
            var correo =  $cookies.get('correo');
            var nombre =  vm.user.nombresUsuario;
            var apellido =  vm.user.apellidosUsuario;
            var rechazo = {origen:id,correo:correo,nombre:nombre,apellido:apellido};
            UserService.RechazardetalledeAlquiler(rechazo)
            .then(function (response) {
                $state.go('talquilo/talquilo-confirm');
                msgflash.success('Se rechazo el alquiler');

                vm.dataLoading= false;
                  
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        Confirmardetalledealquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };

      $scope.showRechazar = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        Rechazardetalledealquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };
    }

})();
