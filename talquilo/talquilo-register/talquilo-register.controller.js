(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloRegisterController', TalquiloRegisterController);

    TalquiloRegisterController.$inject = ['$state','$scope','UserService','$mdToast','$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TalquiloRegisterController($state,$scope,UserService,$mdToast,$location, $rootScope, FlashService,msgflash,$cookies,$q) {

        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("talquilo");  
        vm.querySearch   = querySearch;
        vm.alquilo={arrendador:{}};
        vm.registrarAlquiler = registrarAlquiler;
        vm.showSimpleToastDni=showSimpleToastDni;
        vm.ObtenerDataUsuarioporDni = ObtenerDataUsuarioporDni;
        
        loadCurrentUser();
        loadData();
    
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            
        }

        function registrarAlquiler() {
            vm.progress = true;
            if (vm.alquilo.arrendatario != null) {
                $cookies.putObject('alquilo', vm.alquilo);
                /*var favoriteCookie = $cookies.getObject('alquilo');
                console.log('aqui meimpimre el alquilos');
                console.log(favoriteCookie);*/
                $state.go('talquilo/talquilo-register.detail');
                vm.progress = false;
            }else{
                showSimpleToastDni();
                msgflash.error('Debe de seleccionar un Arrendatario', false);

            }
            
           
        }

         function ObtenerDataUsuarioporDni($event) {
            msgflash.clear();
            if (vm.alquilo.arrendatario != null) {
                $scope.visible=true;
                vm.progress = true;
                UserService.ObtenerDataUsuarioporDniAlquiler(vm.alquilo.arrendatario.idUsuario)
                .then(function (response) {
                    vm.alquilo.arrendatario=response.arrendatario;
                    ObtenerAlquilerUsuario();
                    vm.progress = false;

                });

            }else{
                $scope.visible = false;
                $event.stopPropagation();
                showSimpleToastDni();
                msgflash.error('Debe de seleccionar un Arrendatario', false);

            }
            



        }


        function ObtenerAlquilerUsuario (){
            UserService.ObtenerAlquilerUsuario(vm.alquilo.arrendatario.idUsuario)
            .then(function (response) {
                vm.alquilousuario= response;
                
                console.log('arrendatario idUsuario');
                console.log(vm.alquilousuario);
            });
        }

       //Load data default
        function loadData() {

            UserService.ObtenerPeriodicidad()
            .then(function (response) {
                vm.periodos = response;

                console.log(response);
            });
            UserService.ObtenerProductos()
            .then(function (response) {
                vm.productos = response;

                console.log(response);
            });
            
            vm.moneda = [
                {codigo: 'S', descripcion: 'Soles'},
                {codigo: 'D', descripcion: 'Dólares'}
            ]
            vm.periodo_tasa = [
                {codigo: '1', descripcion: 'Mensual'},
                {codigo: '2', descripcion: 'Anual'}
            ]

            if (obtenernombre == 'talquilo') { 
                var alquilo = $cookies.getObject('alquilo');
                vm.alquilo=alquilo;  
            }

            vm.alquilo.arrendador=vm.user;


        }
          vm.sync_method;
        function querySearch(query) {

              UserService.DatosBusqueda(query)
             .then(function (response) {

                vm.sync_method.resolve(response);
            });

              vm.sync_method=$q.defer();
             return vm.sync_method.promise;
             
        }

        function showSimpleToastDni() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Debe de seleccionar un Arrendatario')
            .position(pinTo )
            .hideDelay(3000)
            );
        }
        
      
    }

})();
