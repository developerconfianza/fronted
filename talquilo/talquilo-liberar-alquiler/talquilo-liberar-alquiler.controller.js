(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloLiberarAlquilerController', TalquiloLiberarAlquilerController);

    TalquiloLiberarAlquilerController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TalquiloLiberarAlquilerController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadConfirmarAlquilerDetalle();
        

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarAlquilerDetalle() {

            UserService.LiberarAlquiler( vm.user.idUsuario)
            .then(function (response) {
              
                vm.liberaralquilerdetail=response;


            });
        }
      
    }

})();
