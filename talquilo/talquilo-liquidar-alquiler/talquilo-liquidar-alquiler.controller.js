(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloliquidarAlquilerController', TalquiloliquidarAlquilerController);

    TalquiloliquidarAlquilerController.$inject = ['$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TalquiloliquidarAlquilerController($scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$q) {
        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("liquidaralquiler");
        vm.querySearch   = querySearch;
        vm.ObtenerDataDni = ObtenerDataDni;
        loadCurrentUser();
        Loadalquileres();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function ObtenerDataDni() {
            var nombrearrendatario= vm.liquidar;
            if (nombrearrendatario != null) {
            var idalquiler= vm.liquidar.idAlquiler;
            var usuario= vm.user.idUsuario;
            var liquidar = {idarrendador:usuario,idarrendatario:vm.liquidar.idUsuario,idalquiler:idalquiler};

            UserService.Obtenerdatospordnialquilerr(liquidar)
            .then(function (response) {

                console.log('desde aqui empieza');
                console.log(response);

                $cookies.put('idAlquiler', response.dnialquiler.idAlquiler);
                $cookies.put('correobeneficiario', response.dnialquiler.correoUsuario);
                 $cookies.put('nombrearrendatario', vm.liquidar.nombresUsuarios);

                console.log('datos por dni');
             
                Obteneralquiler();
            });


            }
            
        }
        $scope.visible=false;
        function Obteneralquiler(){
            $scope.visible=true;
              var idalquiler= $cookies.get('idAlquiler');
             UserService.Obteneralquiler(idalquiler)
            .then(function (response) {
                vm.detallealquiler=response;
                $cookies.putObject('liquidaralquiler', response);
                console.log('datos alquilers aqui');
                console.log(response);
            });

        }

        function Loadalquileres(){
            if (obtenernombre == 'liquidaralquiler') {
                var alquiler = $cookies.getObject('liquidaralquiler');
                var nombrearrendatario = $cookies.get('nombrearrendatario');
                vm.detallealquiler=alquiler;
                vm.liquidar=nombrearrendatario;
            }
        }

        vm.sync_method;
        function querySearch(query) {

            var idUsuario= vm.user.idUsuario;
            var data= {palabra:query,idusuario:idUsuario};
            UserService.BuscarArrendatario(data)
            .then(function (response) {
                console.log('arrendatario');
                console.log(response);

                vm.sync_method.resolve(response);
            });

              vm.sync_method=$q.defer();
             return vm.sync_method.promise;
             
        }


    }

})();
