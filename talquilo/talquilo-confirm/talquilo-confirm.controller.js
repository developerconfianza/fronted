(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloConfirmarController', TalquiloConfirmarController);

    TalquiloConfirmarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TalquiloConfirmarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadConfirmarAlquiler();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarAlquiler() {

            UserService.ConfirmarAlquiler(vm.user.idUsuario)
            .then(function (response) {
                vm.confirmaralquiler=response;
            });
        } 
    }

})();
