(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquilopagarAlquilerDetailController', TalquilopagarAlquilerDetailController);

    TalquilopagarAlquilerDetailController.$inject = ['$stateParams','$state','$mdDialog','$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TalquilopagarAlquilerDetailController($stateParams,$state,$mdDialog,$scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        
        vm.PagarAlquiler = PagarAlquiler;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        msgflash.clear();
        function PagarAlquiler(){
            vm.dataLoading = true;
            var pago= vm.pago;
            var nombre=vm.user.nombresUsuario;
            var apellido=vm.user.apellidosUsuario;
            var correoarrendador=$cookies.get('correoarrendador');
            
            var pagos = {idpagoalquiler:id,pago:pago,correo:correoarrendador,nombre:nombre,
                        apellido:apellido};
            
            UserService.PagarAlquiler(pagos)
            .then(function (response) {
                $state.go('talquilo/talquilo-pagar-alquiler');
                msgflash.success('Se Pago el alquiler');           
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        PagarAlquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción';
           console.log($scope.status);
        });
      };
    }

})();