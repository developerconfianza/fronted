(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquilomisAlquilerController', TalquilomisAlquilerController);

    TalquilomisAlquilerController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TalquilomisAlquilerController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadMisAlquileres();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMisAlquileres() {

            UserService.Resumenalquilerrecibidosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.alquilerrecsoles=response;
            });

            UserService.Resumenalquilerrecibidodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.alquilerrecdolar=response;
            });

            UserService.Resumenalquilerrealizadosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.alquilerreasoles=response;
            });

            UserService.Resumenalquilerrealizadodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.alquilerreadolar=response;
            });
        
        } 
    }

})();
