(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloRegisterDetailController', TalquiloRegisterDetailController);

    TalquiloRegisterDetailController.$inject = ['$state','$http','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope','$mdDialog'];
    function TalquiloRegisterDetailController($state,$http,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope,$mdDialog) {
        var vm = this;

       vm.GuardarAlquiler = GuardarAlquiler;
       loadprestamo();
        
        function loadprestamo(){
            vm.user = $rootScope.globals.user;
            vm.alquilo= $rootScope.globals.alquilo;
            var alquilo = $cookies.getObject('alquilo');
            vm.alquilo=alquilo;
            var actual = new Date();
            vm.fecha=(actual.getDate() + "/" + (actual.getMonth() +1) + "/" + actual.getFullYear());
        }

        msgflash.clear();
        function GuardarAlquiler(){
            msgflash.clear();

            vm.dataLoading = true;

            UserService.RegistrarAlquiler(vm.alquilo)
            .then(function(response){
                msgflash.clear();
                $state.go('talquilo/talquilo-register');
                $cookies.remove('alquilo');
                msgflash.success('Se guardo su Alquiler');

            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        GuardarAlquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };
        
    
          
    }

})();
