(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquiloLiberarAlquilerDetailController', TalquiloLiberarAlquilerDetailController);

    TalquiloLiberarAlquilerDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog','$scope'];
    function TalquiloLiberarAlquilerDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog,$scope) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadLiberarAlquilerDetalle() ;
        
        vm.LevantarAlquiler = LevantarAlquiler;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        
        function loadLiberarAlquilerDetalle() {

            UserService.LiberardetalleAlquiler(id)
            .then(function (response) {
                vm.liberaralquiler=response.liberado;
            });
        }

        msgflash.clear();
        function LevantarAlquiler(){
            vm.dataLoading = true;
            var observacion;
            observacion="";
            if(vm.liberar)
            observacion= vm.liberar.observacion;
            var levantar = {idalquiler:id,observacion:observacion};

            UserService.ActualizarLiberardeAlquiler(levantar)
            .then(function (response) {
                $location.path('/talquilo/talquilo-liberar-alquiler');   
                msgflash.success('Se levanto el alquiler');
 
                  
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        LevantarAlquiler();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };
      
    }

})();
