(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquilomisAlquilerDetailController', TalquilomisAlquilerDetailController);

    TalquilomisAlquilerDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope'];
    function TalquilomisAlquilerDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope) {
        var vm = this;

        var moneda = {moneda:$stateParams.moneda };
        loadCurrentUser();
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            vm.nombre = $rootScope.globals.recibido;
        }

        Cargardetallealquiler();
        SetearNombreDetail(); 

        function Cargardetallealquiler() {
            var moneda = {moneda:$stateParams.moneda };
            var usuario= vm.user.idUsuario;
            var moneda = {moneda:moneda,usuario:usuario};

            vm.dataLoading = true
            UserService.DetalleAlquiler(moneda)
            .then(function(response) {
                vm.detallealquiler = response;
                console.log('me llega datos de moenda');
                console.log(response); 

            });
        };

        function SetearNombreDetail(){
            var moneda = {moneda:$stateParams.moneda };
            var recibido = 'Recibidos';
            var realizado = 'Realizados';
            $cookies.put('recibid', recibido);
            $cookies.put('realizad', realizado);
            
            if (moneda.moneda == 1) {
                var recibid = $cookies.get('recibid');
                vm.alquiler=recibid;    
                  
            }else if (moneda.moneda == 2){
                var recibid = $cookies.get('recibid');
                vm.alquiler=recibid;  

            }
            else{
                var realizad = $cookies.get('realizad');
                vm.alquiler=realizad;
            }             
        }
   
    }

})();
