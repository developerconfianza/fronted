(function () {
    'use strict';

    angular
        .module('app')
        .controller('TalquilopagarAlquilerController', TalquilopagarAlquilerController);

    TalquilopagarAlquilerController.$inject = ['$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TalquilopagarAlquilerController($scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$q) {
        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("pagaralquiler");
        vm.querySearch   = querySearch;
        vm.ObtenerDataDni = ObtenerDataDni;
        loadCurrentUser();
        Loadalquileres();
        

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function ObtenerDataDni() {
            var nombrearrendador = vm.pagar;
            if (nombrearrendador != null) {
            var idalquiler = vm.pagar.idAlquiler;

            var usuario= vm.user.idUsuario;
            var pago = {nombrearrendador:vm.pagar.idUsuario,idarrendatario:usuario,idalquiler:idalquiler};
            UserService.Obtenerdatospordnialquileres(pago)
            .then(function (response) {

                console.log('desde aqui empieza');
                console.log(response);
                $cookies.put('datauserdni', response.pagaralquiler.idAlquiler);
                $cookies.put('correoarrendador', response.pagaralquiler.correoUsuario)
                $cookies.put('nombrearrendador', vm.pagar.nombresUsuarios);
                
                Obtenerpagosalquiler();
            });

            }
            
        }
        $scope.visible=false;
        function Obtenerpagosalquiler(){
            $scope.visible=true;  
            var idalquiler= $cookies.get('datauserdni');
            UserService.Obtenerpagosalquiler(idalquiler)
            .then(function (response) {
                console.log('pagossss');
                console.log(response);
                vm.detallealquiler=response;
                $cookies.putObject('pagaralquiler', response);
                console.log('datos cuotas aqui');
                console.log(response);
            });

        }

        function Loadalquileres(){
            if (obtenernombre == 'pagaralquiler') {
                var alquiler = $cookies.getObject('pagaralquiler');
                var nombrearrendador = $cookies.get('nombrearrendador');
                vm.detallealquiler=alquiler;
                vm.pagar=nombrearrendador;
            }
        }

         vm.sync_method;
        function querySearch(query) {
            var idUsuario= vm.user.idUsuario;
            var data= {palabra:query,idusuario:idUsuario};

            UserService.BuscarArrendador(data)
            .then(function (response) {
                console.log('response');
                console.log(response);

                vm.sync_method.resolve(response);
            });

              vm.sync_method=$q.defer();
             return vm.sync_method.promise;
             
        }

        
    }

})();