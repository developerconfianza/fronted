﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService','$mdToast', '$location', '$rootScope', 'FlashService','msgflash','$scope'];
    function RegisterController(UserService,$mdToast, $location, $rootScope, FlashService,msgflash,$scope) {
        var vm = this;
       
        loadDepartamentos();
        vm.departamentos=[];
        vm.user=[];
        vm.register = register;
        vm.obtenerdatospordni=obtenerdatospordni;
        vm.cambiarprovincias = cambiarprovincias;
        vm.cambiardistritos = cambiardistritos;
        vm.showSimpleToast = showSimpleToast;
        vm.showSimpleToastDni = showSimpleToastDni;

        function register() {

            msgflash.clear();
            vm.dataLoading = true;
            UserService.GuardarUsuario(vm.user)
            .then(function (response) {
                msgflash.clear();
                    if (response.success) {
                        msgflash.success('usuario registrado', true);
                        $location.path('/cuenta');
                    } else {
                        msgflash.error(response.error, false);
                        vm.dataLading = false;
                    }
            });
        }

        function obtenerdatospordni(){

            msgflash.clear();
            if (vm.user.tipoDocumento == 1) {
                if (vm.user.dniUsuario != null) {
                    UserService.ObtenerDatosporDniIngresado(vm.user.dniUsuario)
                    .then(function (response) {
                    msgflash.clear();
                       if (response.success) {
                           vm.user=response.usuario;
                           loadDepartamentos();
                           cambiarprovincias();
                           cambiardistritos();
                        } else {
                            showSimpleToast();
                            vm.dataLading = false;
                        }  
                    });

                }else{
                   showSimpleToast();
                }
            }else{
                showSimpleToastDni();
            }
            
                   vm.user.nombresUsuario='';
                    vm.user.apellidosUsuario='';
                    vm.user.idDepartamento='';
                    vm.user.idProvincia='';
                    vm.user.idDistrito='';
                    vm.user.fechaNacimiento='';
                    vm.user.sexoUsiario='';   
            

        }

        function loadDepartamentos() {

             UserService.ObtenerDepartamentos()
            .then(function (response) {
                vm.departamentos = response;
                console.log(response);
            });


            vm.tipo_documento = [
                {codigo: '1', descripcion: 'DNI'},
                {codigo: '2', descripcion: 'Carnet de Extranjería'},
                {codigo: '3', descripcion: 'Otros'}
            ]

            vm.sexo = [
                {codigo: '2', descripcion: 'Femenino'},
                {codigo: '1', descripcion: 'Masculino'}
            ]

        }

        function cambiarprovincias(){
            UserService.ObtenerProvincias(vm.user.idDepartamento)
            .then(function (response) {
                vm.provincias = response;
            });
        }

        function cambiardistritos(){
            UserService.ObtenerDistritos(vm.user.idProvincia)
            .then(function (response) {
                vm.distritos = response;
                
            });
        } 

        function showSimpleToast() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Debe de Ingresar un DNI vàlido')
            .position(pinTo )
            .hideDelay(3000)
            );
        }

        function showSimpleToastDni() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Solo se puede validar con DNI')
            .position(pinTo )
            .hideDelay(3000)
            );
        }

    }

})();
