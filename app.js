    (function () {
    'use strict';
      //var initialized = false;
    angular
        .module('app', ['ngRoute', 'ngCookies', 'kinvey',
      'ngMessages',
      'ngMaterial',
      'ngAnimate',
      'ngAria',
      'ngResource',
      'ngStorage',
      'ngMask',
      'ui.bootstrap',
      'ngDialog',
      'blockUI','ui.router',
      'ui.utils.masks'])

  
   .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  
            $stateProvider.state('login', {
              url: '/',
              templateUrl: 'login/login.view.html',
              controller: 'LoginController',
              controllerAs: 'vm'
            })

             .state('buscar', {
              url: '/buscar',
                controller: 'BuscarController',
                templateUrl: 'buscar/buscar.view.html',
                controllerAs: 'vm'
            })

            .state('cuenta', {
                 url: '/cuenta',
                
               controller: 'CuentaController',
                templateUrl: 'cuenta/cuenta.view.html',
                controllerAs: 'vm'
            })
            .state('activar-cuenta', {
                 url: '/activar-cuenta',
                
               controller: 'ActivarCuentaController',
                templateUrl: 'activar-cuenta/activar-cuenta.view.html',
                controllerAs: 'vm'
            })

            .state('dashboard', {
                 url: '/dashboard',
                controller: 'DashboardController',
                templateUrl: 'dashboard/dashboard.view.html',
                controllerAs: 'vm'
            })

             .state('correo', {
                 url: '/correo',
                controller: 'CorreoController',
                templateUrl: 'correo/correo.view.html',
                controllerAs: 'vm'
            })

              .state('actualizar-contrasena', {
                 url: '/actualizar-contrasena',
                controller: 'ContrasenaController',
                templateUrl: 'actualizar-contrasena/contrasena.view.html',
                controllerAs: 'vm'
            })

              .state('error-token', {
                 url: '/error-token',
                controller: 'ErrorController',
                templateUrl: 'error-token/error.view.html',
                controllerAs: 'vm'
            })

              .state('registrar-usuario', {
                 url: '/registrar-usuario',
                controller: 'RegisterController',
                templateUrl: 'registrar-usuario/register.view.html',
                controllerAs: 'vm'
            })

              .state('editar-usuario', {
                 url: '/editar-usuario',
                controller: 'EditarController',
                templateUrl: 'editar-usuario/editar.view.html',
                controllerAs: 'vm'
            })

              .state('mi-red', {
                 url: '/mi-red',
                controller: 'MiredController',
                templateUrl: 'mi-red/mi-red.view.html',
                controllerAs: 'vm'
            })

              .state('favoritos', {
                 url: '/favoritos',
                controller: 'FavoritosController',
                templateUrl: 'favoritos/favoritos.view.html',
                controllerAs: 'vm'
            })
            .state('tpresto/tpresto-mis-prestamos', {
                 url: '/tpresto/tpresto-mis-prestamos',
                controller: 'TprestomisPrestamoController',
                templateUrl: 'tpresto/tpresto-mis-prestamos/tpresto-mis-prestamos.view.html',
                controllerAs: 'vm'
            })

            .state('tpresto/tpresto-mis-prestamos.detail', {
                url: '/detail/:moneda',
                views: {
                '@': {
                controller: 'TprestomisPrestamoDetailController',
                templateUrl: 'tpresto/tpresto-mis-prestamos-detail/tpresto-mis-prestamos-detail.view.html',
                controllerAs: 'vm'
                    }
                }
            })

            .state('tpresto/tpresto-register', {
                url: '/tpresto/tpresto-register',
                controller: 'TprestoRegisterController',
                templateUrl: 'tpresto/tpresto-register/tpresto-register.view.html',
                controllerAs: 'vm'
             })
            .state('tpresto/tpresto-register.detail', {
                url: '/detail/',
                views: {
                '@': {
                controller: 'TprestoRegisterDetailController',
                templateUrl: 'tpresto/tpresto-register-detail/tpresto-register-detail.view.html',
                controllerAs: 'vm'
                     }
                }
             })

            .state('tpresto/tpresto-confirm', {
                url: '/tpresto/tpresto-confirm',
                controller: 'TPrestoConfirmarController',
                templateUrl: 'tpresto/tpresto-confirm/tpresto-confirm.view.html',
                controllerAs: 'vm'
            })

            .state('tpresto/tpresto-confirm.detail', {
                url: '/detail/:id',
                views: {
                '@': {
                controller: 'TprestoConfirmarPrestamoController',
                templateUrl: 'tpresto/tpresto-confirm-detail/tpresto-confirm-detail.view.html',
                controllerAs: 'vm'
                }
              }  
            })

            .state('tpresto/tpresto-liberar-prestamo', {
                url: '/tpresto/tpresto-liberar-prestamo',
                controller: 'TprestoLiberarPrestamoController',
                templateUrl: 'tpresto/tpresto-liberar-prestamo/tpresto-liberar-prestamo.view.html',
                controllerAs: 'vm'
            })

            .state('tpresto/tpresto-liberar-prestamo.detail', {
                url: '/detail/:id',
                views: {
                '@': {
                controller: 'TprestoLiberarPrestamoDetailController',
                templateUrl: 'tpresto/tpresto-liberar-prestamo-detail/tpresto-liberar-prestamo-detail.view.html',
                controllerAs: 'vm'
                     }
                }
            })
         
            .state('tpresto/tpresto-liquidar-cuentas', {
                 url: '/tpresto/tpresto-liquidar-cuentas',
                controller: 'TprestoliquidarCuentasController',
                templateUrl:'tpresto/tpresto-liquidar-cuentas/tpresto-liquidar-cuentas.view.html',
                controllerAs: 'vm'
            })

            .state('tpresto/tpresto-liquidar-cuentas.detail', {
                url: '/detail/:id',
                views: {
                '@': {
                controller: 'TprestoliquidarCuentasDetailController',
                templateUrl:'tpresto/tpresto-liquidar-cuentas-detail/tpresto-liquidar-cuentas-detail.view.html',
                controllerAs: 'vm'
                     }
                }
            })

            .state('tpresto/tpresto-pagar-cuotas', {
                 url: '/tpresto/tpresto-pagar-cuotas',
                controller: 'TprestopagarCuotasController',
                templateUrl:'tpresto/tpresto-pagar-cuotas/tpresto-pagar-cuotas.view.html',
                controllerAs: 'vm'
            })

             .state('tpresto/tpresto-pagar-cuotas.detail', {
                url: '/detail/:id',
                 views: {
                '@': {
                controller: 'TprestopagarCuotasDetailController',
                templateUrl:'tpresto/tpresto-pagar-cuotas-detail/tpresto-pagar-cuotas-detail.view.html',
                controllerAs: 'vm'
                    }
                }
            })

            .state('tcontrato/tcontrato-mis-contratos', {
                 url: '/tcontrato/tcontrato-mis-contratos',
                controller: 'TcontratomisContratoController',
                templateUrl: 'tcontrato/tcontrato-mis-contratos/tcontrato-mis-contratos.view.html',
                controllerAs: 'vm'
            })
            .state('tcontrato/tcontrato-mis-contratos.detail', {
                 url: '/detail/:moneda',
                   views: {
                '@': {
                controller: 'TcontratomisContratoDetailController',
                templateUrl: 'tcontrato/tcontrato-mis-contratos-detail/tcontrato-mis-contratos-detail.view.html',
                controllerAs: 'vm'
                        }
                    }
            })

            .state('tcontrato/tcontrato-register', {
                 url: '/tcontrato/tcontrato-register',
                controller: 'TcontratoRegisterController',
                templateUrl: 'tcontrato/tcontrato-register/tcontrato-register.view.html',
                controllerAs: 'vm'
             })
            .state('tcontrato/tcontrato-register.detail', {
                 url: '/detail/',
                views: {
                '@': {
                controller: 'TcontratoRegisterDetailController',
                templateUrl: 'tcontrato/tcontrato-register-detail/tcontrato-register-detail.view.html',
                controllerAs: 'vm'
                       }
                   }
             })

            .state('tcontrato/tcontrato-confirm', {
                 url: '/tcontrato/tcontrato-confirm',
                controller: 'TcontratoConfirmarController',
                templateUrl: 'tcontrato/tcontrato-confirm/tcontrato-confirm.view.html',
                controllerAs: 'vm'
            })

            .state('tcontrato/tcontrato-confirm.detail', {
                 url: '/detail/:id',
                views: {
                '@': {
                controller: 'TcontratoConfirmarContratoController',
                templateUrl: 'tcontrato/tcontrato-confirm-detail/tcontrato-confirm-detail.view.html',
                controllerAs: 'vm'
                        }
                    }
            })

             .state('tcontrato/tcontrato-liberar-contrato', {
                 url: '/tcontrato/tcontrato-liberar-contrato',
                controller: 'TcontratoLiberarContratoController',
                templateUrl: 'tcontrato/tcontrato-liberar-contrato/tcontrato-liberar-contrato.view.html',
                controllerAs: 'vm'
            })

            .state('tcontrato/tcontrato-liberar-contrato.detail', {
                 url: '/detail/:id',
                   views: {
                '@': {
                controller: 'TcontratoLiberarContratoDetailController',
                templateUrl: 'tcontrato/tcontrato-liberar-contrato-detail/tcontrato-liberar-contrato-detail.view.html',
                controllerAs: 'vm'
                         }
                     }
            })

             .state('tvendo/tvendo-mis-ventas', {
                 url: '/tvendo/tvendo-mis-ventas',
                controller: 'TvendomisVentasController',
                templateUrl: 'tvendo/tvendo-mis-ventas/tvendo-mis-ventas.view.html',
                controllerAs: 'vm'
            })
            .state('tvendo/tvendo-mis-ventas.detail', {
                 url: '/detail/:moneda',
                   views: {
                '@': {
                controller: 'TvendomisVentasDetailController',
                templateUrl: 'tvendo/tvendo-mis-ventas-detail/tvendo-mis-ventas-detail.view.html',
                controllerAs: 'vm'
                 }
             }
            })

            .state('tvendo/tvendo-register', {
                 url: '/tvendo/tvendo-register',
                controller: 'TvendoRegisterController',
                templateUrl: 'tvendo/tvendo-register/tvendo-register.view.html',
                controllerAs: 'vm'
             })
            .state('tvendo/tvendo-register.detail', {
                 url: '/detail/',
                   views: {
                '@': {
                controller: 'TvendoRegisterDetailController',
                templateUrl: 'tvendo/tvendo-register-detail/tvendo-register-detail.view.html',
                controllerAs: 'vm'
                         }
                    }
             })

            .state('tvendo/tvendo-confirm', {
                 url: '/tvendo/tvendo-confirm',
                controller: 'TvendoConfirmarController',
                templateUrl: 'tvendo/tvendo-confirm/tvendo-confirm.view.html',
                controllerAs: 'vm'
            })

            .state('tvendo/tvendo-confirm.detail', {
                 url: '/detail/:id',
                   views: {
                '@': {
                controller: 'TvendoConfirmarVentaController',
                templateUrl: 'tvendo/tvendo-confirm-detail/tvendo-confirm-detail.view.html',
                controllerAs: 'vm'
                          }
                      }
            })

             .state('tvendo/tvendo-liberar-venta', {
                 url: '/tvendo/tvendo-liberar-venta',
                controller: 'TvendoLiberarVentaController',
                templateUrl: 'tvendo/tvendo-liberar-venta/tvendo-liberar-venta.view.html',
                controllerAs: 'vm'
            })

            .state('tvendo/tvendo-liberar-venta.detail', {
                 url: '/detail/:id',
                   views: {
                '@': {
                controller: 'TvendoLiberarVentaDetailController',
                templateUrl: 'tvendo/tvendo-liberar-venta-detail/tvendo-liberar-venta-detail.view.html',
                controllerAs: 'vm'
                        }
                    }
            })

      
            .state('talquilo/talquilo-mis-alquileres', {
                 url: '/talquilo/talquilo-mis-alquileres',
                controller: 'TalquilomisAlquilerController',
                templateUrl: 'talquilo/talquilo-mis-alquileres/talquilo-mis-alquileres.view.html',
                controllerAs: 'vm'
            })

            .state('talquilo/talquilo-mis-alquileres.detail', {
                 url: '/detail/:moneda',
                   views: {
                '@': {
                controller: 'TalquilomisAlquilerDetailController',
                templateUrl: 'talquilo/talquilo-mis-alquileres-detail/talquilo-mis-alquileres-detail.view.html',
                controllerAs: 'vm'
                         }
                     }
            })

            .state('talquilo/talquilo-register', {
                 url: '/talquilo/talquilo-register',
                controller: 'TalquiloRegisterController',
                templateUrl: 'talquilo/talquilo-register/talquilo-register.view.html',
                controllerAs: 'vm'
             })
            .state('talquilo/talquilo-register.detail', {
                 url: '/detail/',
                   views: {
                '@': {
                controller: 'TalquiloRegisterDetailController',
                templateUrl: 'talquilo/talquilo-register-detail/talquilo-register-detail.view.html',
                controllerAs: 'vm'
                        }
                    }
             })

            .state('talquilo/talquilo-confirm', {
                 url: '/talquilo/talquilo-confirm',
                controller: 'TalquiloConfirmarController',
                templateUrl: 'talquilo/talquilo-confirm/talquilo-confirm.view.html',
                controllerAs: 'vm'
            })

            .state('talquilo/talquilo-confirm.detail', {
                 url: '/detail/:id',
                   views: {
                '@': {
                controller: 'TalquiloConfirmarAlquilerController',
                templateUrl: 'talquilo/talquilo-confirm-detail/talquilo-confirm-detail.view.html',
                controllerAs: 'vm'
                         }
                     }
            })

            .state('talquilo/talquilo-liberar-alquiler', {
                 url: '/talquilo/talquilo-liberar-alquiler',
                controller: 'TalquiloLiberarAlquilerController',
                templateUrl: 'talquilo/talquilo-liberar-alquiler/talquilo-liberar-alquiler.view.html',
                controllerAs: 'vm'
            })

            .state('talquilo/talquilo-liberar-alquiler.detail', {
                 url: '/detail/:id',
                   views: {
                '@': {
                controller: 'TalquiloLiberarAlquilerDetailController',
                templateUrl: 'talquilo/talquilo-liberar-alquiler-detail/talquilo-liberar-alquiler-detail.view.html',
                controllerAs: 'vm'
                          }
                      }
            })
            .state('talquilo/talquilo-liquidar-alquiler', {
                 url: '/talquilo/talquilo-liquidar-alquiler',
                controller: 'TalquiloliquidarAlquilerController',
                templateUrl:'talquilo/talquilo-liquidar-alquiler/talquilo-liquidar-alquiler.view.html',
                controllerAs: 'vm'
            })

            .state('talquilo/talquilo-liquidar-alquiler.detail', {
                 url: '/detail/:id',
                 views: {
                '@': {
                controller: 'TalquiloliquidarAlquilerDetailController',
                templateUrl:'talquilo/talquilo-liquidar-alquiler-detail/talquilo-liquidar-alquiler-detail.view.html',
                controllerAs: 'vm'
                        }
                    }
            })

            .state('talquilo/talquilo-pagar-alquiler', {
                 url: '/talquilo/talquilo-pagar-alquiler',
                controller: 'TalquilopagarAlquilerController',
                templateUrl:'talquilo/talquilo-pagar-alquiler/talquilo-pagar-alquiler.view.html',
                controllerAs: 'vm'
            })

             .state('talquilo/talquilo-pagar-alquiler.detail', {
                 url: '/detail/:id',
                 views: {
                '@': {
                controller: 'TalquilopagarAlquilerDetailController',
                templateUrl:'talquilo/talquilo-pagar-alquiler-detail/talquilo-pagar-alquiler-detail.view.html',
                controllerAs: 'vm'
                           }
                       }
            });




            //
           

            // END STATE
            $urlRouterProvider.otherwise('/');
            }])
.run(run);


 run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
    function run($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }   

        /*$rootScope.$on('$locationChangeStart', function (event, next, current) {
            //redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login','/buscar','/cuenta','/activar-cuenta','/dashboard','/correo','/actualizar-contrasena','/error-token','/registrar-usuario',
                '/editar-usuario','/mi-red','/favoritos','/tpresto/tpresto-mis-prestamos','/tpresto/tpresto-register','/tpresto/tpresto-register-detail','/tpresto/tpresto-confirm',
                '/tpresto/tpresto-mis-prestamos-detail','/tpresto/tpresto-confirm-detail','/tpresto/tpresto-liberar-prestamo','/tpresto/tpresto-liberar-prestamo-detail','/tpresto/tpresto-liquidar-cuentas',
                '/tpresto/tpresto-liquidar-cuentas-detail','/tpresto/tpresto-pagar-cuotas','/tpresto/tpresto-pagar-cuotas-detail','/tcontrato/tcontrato-mis-contratos','/tcontrato/tcontrato-mis-contratos-detail',
                '/tcontrato/tcontrato-register','/tcontrato/tcontrato-register-detail','/tcontrato/tcontrato-confirm','/tcontrato/tcontrato-confirm-detail','/tcontrato/tcontrato-liberar-contrato','/tcontrato/tcontrato-liberar-contrato-detail',
                '/tvendo/tvendo-mis-ventas','/tvendo/tvendo-mis-ventas-detail','/tvendo/tvendo-register','/tvendo/tvendo-register-detail','/tvendo/tvendo-confirm','/tvendo/tvendo-confirm-detail',
                '/tvendo/tvendo-liberar-venta','/tvendo/tvendo-liberar-venta-detail','/talquilo/talquilo-mis-alquileres','/talquilo/talquilo-mis-alquileres-detail','/talquilo/talquilo-register',
                '/talquilo/talquilo-register-detail','/talquilo/talquilo-confirm','/talquilo/talquilo-confirm-detail','/talquilo/talquilo-liberar-alquiler','/talquilo/talquilo-liberar-alquiler-detail',
                '/talquilo/talquilo-liquidar-alquiler','/talquilo/talquilo-liquidar-alquiler-detail','/talquilo/talquilo-pagar-alquiler','/talquilo/talquilo-pagar-alquiler-detail']) === -1;
        
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/');
            }
        });*/
    }




})();