(function () {
    'use strict';

    angular
        .module('app')
        .controller('ActivarCuentaController', ActivarCuentaController);

    ActivarCuentaController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash'];
    function ActivarCuentaController(UserService, $location, $rootScope, FlashService,msgflash) {
        var vm = this;
        var tokenobtenido = obtenerQuerryStringValue("token");
        var token = {token:tokenobtenido};

        function estado() {

            vm.dataLoading = true
            UserService.VerificarTokenCuenta(token)
            .then(function(response) {
                if (response.estado == true) {
                    $location.path('/activar-cuenta');
                    vm.dataLoading = false;
                }else{
                    $location.path('/error-token');
                }                    
            });
        };

        estado();
        cambiarestado();
 
        function cambiarestado(){

            vm.dataLoading = true;
            var token = {token:tokenobtenido};

            UserService.CambiarEstadoporToken(token)
            .then(function(response){
                 
            });
        };

    }               
})();
