(function () {
    'use strict';

    angular
        .module('app')  
        .controller('FavoritosController', FavoritosController);

    FavoritosController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function FavoritosController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadMiFavoritos();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMiFavoritos() {

            UserService.Mired(vm.user.idUsuario)
            .then(function (response) {
                vm.misfavoritos=response;
            });
        } 
    }

})();
