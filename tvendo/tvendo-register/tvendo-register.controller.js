(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoRegisterController', TvendoRegisterController);

    TvendoRegisterController.$inject = ['$state','$scope','UserService','$mdToast','$location', '$rootScope', 'FlashService','msgflash','$cookies','$q'];
    function TvendoRegisterController($state,$scope,UserService,$mdToast,$location, $rootScope, FlashService,msgflash,$cookies,$q) {

        var vm = this;
        var obtenernombre = obtenerQuerryStringValue("tvendo"); 
        vm.querySearch   = querySearch;
        vm.vendo={vendedor:{}};
        vm.registrarVenta = registrarVenta;
        vm.showSimpleToastDni=showSimpleToastDni;
        vm.ObtenerDataUsuarioporDniVenta = ObtenerDataUsuarioporDniVenta;
        loadCurrentUser();
        loadData(); 

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function registrarVenta() {
            vm.progress = true;
            if (vm.vendo.comprador != null) {
                 $cookies.putObject('vendo', vm.vendo);
                var favoriteCookie = $cookies.getObject('prestamo');
                $state.go('tvendo/tvendo-register.detail');
                vm.progress = false;

            }else{
                showSimpleToastDni();
                msgflash.error('Debe de seleccionar un Comprador', false);

            }
        }

         function ObtenerDataUsuarioporDniVenta($event) {

            msgflash.clear();
            if (vm.vendo.comprador != null) {
                $scope.visible = true;
                vm.progress = true;
                UserService.ObtenerDataUsuarioporDniVenta(vm.vendo.comprador.idUsuario)
                .then(function (response) {
                    vm.vendo.comprador=response.comprador;
                    ObtenerVentaUsuario();
                    vm.progress = false;
                });
            }else{
                $scope.visible = false;
                $event.stopPropagation();
                showSimpleToastDni()
                msgflash.error('Debe de seleccionar un Comprador', false);

            }
    
        }

        function ObtenerVentaUsuario (){

            UserService.ObtenerVentasUsuario(vm.vendo.comprador.idUsuario)
            .then(function (response) {
                vm.ventausuario= response;
            });
        }

       //Load data default
        function loadData() {

            UserService.ObtenerProductos()
            .then(function (response) {
                vm.productos_venta = response;
            });
            
            vm.moneda = [
                {codigo: 'S', descripcion: 'Soles'},
                {codigo: 'D', descripcion: 'Dólares'}
            ]

            if (obtenernombre == 'tvendo') { 
                var vendo = $cookies.getObject('vendo');
                vm.vendo=vendo;
                    
            }

            vm.vendo.vendedor=vm.user;


        }
         
          vm.sync_method;
        function querySearch(query) {

              UserService.DatosBusqueda(query)
             .then(function (response) {
                vm.sync_method.resolve(response);
            });

            vm.sync_method=$q.defer();
            return vm.sync_method.promise;
             
        }

        function showSimpleToastDni() {
            var pinTo = 'top right';
            $mdToast.show(
            $mdToast.simple()
            .textContent('Debe de seleccionar un Comprador')
            .position(pinTo )
            .hideDelay(3000)
            );
        }
        
      
    }

})();
