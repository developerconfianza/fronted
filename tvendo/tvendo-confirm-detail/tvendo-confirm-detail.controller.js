(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoConfirmarVentaController', TvendoConfirmarVentaController);

    TvendoConfirmarVentaController.$inject = ['$stateParams','$state','$scope','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog'];
    function TvendoConfirmarVentaController( $stateParams,$state,$scope,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog) {
        var vm = this;
        var id = {id:$stateParams.id };
       

        loadCurrentUser();
        loadConfirmarVentaDetalle();
        vm.ConfirmardetalledeVenta = ConfirmardetalledeVenta;
        vm.RechazardetalledeVenta=RechazardetalledeVenta;
     
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarVentaDetalle() {

            UserService.ConfirmarVentaDetalle(id)
            .then(function (response) {
                vm.confirmarventadetail=response.confirmado;
                $cookies.put('correovendedor', response.confirmado.correoUsuario);
                $cookies.put('nombrevendedor', response.confirmado.nombresUsuario);
                $cookies.put('apellidovendedor', response.confirmado.apellidosUsuario);
            });
        } 
        msgflash.clear();
        function ConfirmardetalledeVenta(){
            vm.dataLoading = true;  
            var correovendedor =  $cookies.get('correovendedor');
            var nombrevendedor =  $cookies.get('nombrevendedor');
            var apellidovendedor =  $cookies.get('apellidovendedor');
            var nombrecomprador =  vm.user.nombresUsuario;
            var apellidocomprador =  vm.user.apellidosUsuario;
            var correocomprador=vm.user.correoUsuario;
            var confirmado = {origen:id,correovendedor:correovendedor,nombrevendedor:nombrevendedor,
              apellidovendedor:apellidovendedor,nombrecomprador:nombrecomprador,apellidocomprador:apellidocomprador,
              correocomprador:correocomprador}; 
            UserService.ConfirmardetalledeVenta(confirmado)
            .then(function (response) {
                $state.go('tvendo/tvendo-confirm');
                msgflash.success('Se confirmo la venta');
                vm.dataLoading= false;   
            });
        }

        function RechazardetalledeVenta(){
            vm.dataLoading = true;
            var correovendedor =  $cookies.get('correovendedor');
            var nombrevendedor =  $cookies.get('nombrevendedor');
            var apellidovendedor =  $cookies.get('apellidovendedor');
            var nombrecomprador =  vm.user.nombresUsuario;
            var apellidocomprador =  vm.user.apellidosUsuario;
            var correocomprador=vm.user.correoUsuario;
            var rechazado = {origen:id,correovendedor:correovendedor,nombrevendedor:nombrevendedor,
              apellidovendedor:apellidovendedor,nombrecomprador:nombrecomprador,apellidocomprador:apellidocomprador,
              correocomprador:correocomprador};   
            UserService.RechazardetalledeVenta(rechazado)
            .then(function (response) {
                $state.go('tvendo/tvendo-confirm');
                msgflash.success('Se rechazo la venta');

                vm.dataLoading= false;

              
            });
        }

        $scope.showConfirm = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        ConfirmardetalledeVenta();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
           console.log($scope.status);
        });
      };

      $scope.showRechazar = function(ev) {
        $scope.status = '  ';
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

        $mdDialog.show(confirm)
        .then(function() {
        RechazardetalledeVenta();
        },
        function() {
          $scope.status = 'No se realizo la acción.';
        });
      };

    }

})();
