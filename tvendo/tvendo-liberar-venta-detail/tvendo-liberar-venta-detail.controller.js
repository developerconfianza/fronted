(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoLiberarVentaDetailController', TvendoLiberarVentaDetailController);

    TvendoLiberarVentaDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$mdDialog','$scope'];
    function TvendoLiberarVentaDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$mdDialog,$scope) {
        var vm = this;
        var id = {id:$stateParams.id };
        loadCurrentUser();
        loadLiberarVentaDetalle() ;
        
        vm.LevantarVenta = LevantarVenta;
        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadLiberarVentaDetalle() {

            UserService.LiberardetalleVenta(id)
            .then(function (response) {
                vm.liberarventa=response.liberado;
            });
        }

        msgflash.clear();
        function LevantarVenta(){
            vm.dataLoading = true;
            var observacion;
            observacion="";
            if(vm.liberar)
            observacion= vm.liberar.observacion;
            var levantar = {idventa:id,observacion:observacion};

            UserService.ActualizarLiberardeVenta(levantar)
            .then(function (response) {
                $location.path('/tvendo/tvendo-liberar-venta');
                msgflash.success('Se levanto el contrato');
                
            });
        }

        $scope.showConfirm = function(ev) {
            $scope.status = '  ';
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

            $mdDialog.show(confirm)
            .then(function() {
            LevantarVenta();
            },
            function() {
            $scope.status = 'No se realizo la acción.';
            });
        };
      
    }

})();
