(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoRegisterDetailController', TvendoRegisterDetailController);

    TvendoRegisterDetailController.$inject = ['$state','$http','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope','$mdDialog'];
    function TvendoRegisterDetailController($state,$http,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope,$mdDialog) {
        var vm = this;

       vm.GuardarVenta = GuardarVenta;
       loadprestamo();
        

        function loadprestamo(){
            vm.user = $rootScope.globals.user;
            var vendo = $cookies.getObject('vendo');
            vm.vendo=vendo;
        }

        msgflash.clear();
        function GuardarVenta(){
            msgflash.clear();
            vm.dataLoading = true;
            UserService.RegistrarVenta(vm.vendo)
            .then(function(response){ 
                msgflash.clear();
                $state.go('tvendo/tvendo-register');
                $cookies.remove('vendo');
                msgflash.success('Se guardo su venta');
            });
        } 

        $scope.showConfirm = function(ev) {
          $scope.status = '  ';
          // Appending dialog to document.body to cover sidenav in docs app
          var confirm = $mdDialog.confirm()
              .title('Mensaje de Confirmación')
              .textContent('¿Está seguro de continuar con el proceso?')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('SI')
              .cancel('NO');

          $mdDialog.show(confirm)
          .then(function() {
          GuardarVenta();
          },
          function() {
          $scope.status = 'No se realizo la acción.';
          
          });
        };       
          
    }

})();
