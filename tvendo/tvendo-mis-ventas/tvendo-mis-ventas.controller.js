(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendomisVentasController', TvendomisVentasController);

    TvendomisVentasController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TvendomisVentasController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadMisVentas();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadMisVentas() {

            UserService.Resumenventarecibidosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.ventarecsoles=response;
            });

            UserService.Resumenventarecibidodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.ventarecdolar=response;
            });

            UserService.Resumenventarealizadosoles(vm.user.idUsuario)
            .then(function (response) {
                vm.ventareasoles=response;
            });

            UserService.Resumenventarealizadodolares(vm.user.idUsuario)
            .then(function (response) {
                vm.ventareadolar=response;
            });
        
        } 
    }

})();
