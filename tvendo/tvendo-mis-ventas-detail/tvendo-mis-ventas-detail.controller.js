(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendomisVentasDetailController', TvendomisVentasDetailController);

    TvendomisVentasDetailController.$inject = ['$stateParams','UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies','$scope'];
    function TvendomisVentasDetailController($stateParams,UserService, $location, $rootScope, FlashService,msgflash,$cookies,$scope) {
        var vm = this;
        var moneda = {moneda:$stateParams.moneda };
        loadCurrentUser();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
            vm.nombre = $rootScope.globals.recibido;
        }

        Cargardetalleventa();
        SetearNombreDetail(); 

        function Cargardetalleventa() {
            var moneda = {moneda:$stateParams.moneda };
            var usuario= vm.user.idUsuario;
            var moneda = {moneda:moneda,usuario:usuario};

            vm.dataLoading = true
            UserService.DetalleVenta(moneda)
            .then(function(response) {
                vm.detalleventa = response;
            });
        };

        function SetearNombreDetail(){
            var moneda = {moneda:$stateParams.moneda };
            var recibido = 'Recibidos';
            var realizado = 'Realizados';
            $cookies.put('recibidoventa', recibido);
            $cookies.put('realizadoventa', realizado);
            
            if (moneda.moneda == 1) {
                var recibidoventa = $cookies.get('recibidoventa');
                vm.ventas=recibidoventa;   
              
            }else if (moneda.moneda == 2){
                var recibidoventa = $cookies.get('recibidoventa');
                vm.ventas=recibidoventa; 
            }
            else{
                var realizadoventa = $cookies.get('realizadoventa');
                vm.ventas=realizadoventa; 
            }             
        }
   
    }

})();
