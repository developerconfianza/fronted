(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoConfirmarController', TvendoConfirmarController);

    TvendoConfirmarController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TvendoConfirmarController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;

        loadCurrentUser();
        loadConfirmarVenta();

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarVenta() {

            UserService.ConfirmarVenta(vm.user.idUsuario)
            .then(function (response) {
                vm.confirmarventa=response;
            });
        } 
    }

})();
