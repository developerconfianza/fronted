(function () {
    'use strict';

    angular
        .module('app')
        .controller('TvendoLiberarVentaController', TvendoLiberarVentaController);

    TvendoLiberarVentaController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash','$cookies'];
    function TvendoLiberarVentaController(UserService, $location, $rootScope, FlashService,msgflash,$cookies) {
        var vm = this;
        loadCurrentUser();
        loadConfirmarVentaDetalle() ;
        

        function loadCurrentUser() {
            vm.user = $rootScope.globals.user;
        }

        function loadConfirmarVentaDetalle() {

            UserService.LiberarVenta(vm.user.idUsuario)
            .then(function (response) {
              
                vm.liberarventadetail=response;


            });
        }
      
    }

})();
