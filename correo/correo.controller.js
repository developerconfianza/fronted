(function () {
    'use strict';

    angular
        .module('app')
        .controller('CorreoController', CorreoController);

        
    CorreoController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash'];
    function CorreoController(UserService, $location, $rootScope, FlashService,msgflash) {
        var vm = this
        vm.correo = correo;
             
        function correo() {
            vm.dataLoading = true;    
            UserService.VerificarCorreo(vm.user)
                .then(function (response) {
                      msgflash.clear();
                    if (response.success) {
                        msgflash.success('Se ha enviado un correo electrónico a la dirección proporcionada con las instrucciones para reestablecer tu contraseña. ', true);
                        
                    } else {
                        msgflash.error('no se pudo encontrar este correo , inserte uno valido', false);
                        
                       vm.dataLoading = false;
                    }
                });
        }

    }
     
})();
