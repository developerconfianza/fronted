(function () {
    'use strict';

    angular
        .module('app')
        .controller('ContrasenaController', ContrasenaController);

    ContrasenaController.$inject = ['UserService', '$location', '$rootScope', 'FlashService','msgflash'];
    function ContrasenaController(UserService, $location, $rootScope, FlashService,msgflash) {
        var vm = this;
        var tokenobtenido = obtenerQuerryStringValue("token");
        var token = {token:tokenobtenido};

        vm.password = password;
        vm.cambiarpassword = cambiarpassword;

        function password() {

            vm.dataLoading = true
            UserService.VerificarToken(token)
            .then(function(response) {

                if (response.estado == true) {
                    $location.path('/actualizar-contrasena');
                    vm.dataLoading = false;
                }else{
                    $location.path('/error-token');

                }                    
            });
        };

        password();
 
        function cambiarpassword(){

            vm.dataLoading = true;
            var password =  vm.user.password;
            var passwordconfirmado =  vm.user.password1;
            var token = {token:tokenobtenido,password:password,passwordconfirmado:passwordconfirmado};

            UserService.CambiarPasswordporToken(token)
            .then(function(response){
                 msgflash.clear();

                    if (response.success) {
                        msgflash.success('Se actualizo la contraseña ', true);
                        $location.path('/login');
                       
                    } else {
                        msgflash.error('Las contraseñas no coinciden  ', false);
                        vm.dataLoading = false;
                    }
            });
        };

    }               
})();
