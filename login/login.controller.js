﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$http','UserService','$location', 'FlashService','msgflash','$rootScope','$cookies'];
    function LoginController($http,UserService,$location, FlashService,msgflash,$rootScope,$cookies) {
        
        var vm = this;
        msgflash.clear();
        vm.login = login; 
        $rootScope.globals = {};

        (function initController() {

            ClearCredentials();
        })();

     
        function login() {
           
            vm.dataLoading = true;

            var email = vm.email;
            var authdata = Base64_encode(email);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
           
            UserService.Autenticar(authdata,vm.password)
            .then (function (response) {
                if (response.estado == true ) {
                   
                    $rootScope.globals.user = response.user;
                    var cookieExp = new Date();
                    cookieExp.setDate(cookieExp.getDate() + 7);
                    $cookies.putObject('globals', $rootScope.globals, { expires: cookieExp });
                    $location.path('/dashboard');
                } else {
                      
                   msgflash.errorlogin(response.error, false);
                   vm.dataLading = false;
                }
   
            });
          
        }

        function ClearCredentials() {
            
            $cookies.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }
    }

})();
